<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$description = $config[description_google];
$keywords = $config[keywords_google];
$titulo_pagina = $config[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>





  .bg-interna{
    background:  url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>




</head>

<body class="bg-interna">


  <?php  require_once("../includes/topo.php")?>

  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row clearfix ">
      <div class="">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/bg-dicas.jpg"
          width="380"
          height="160"
          layout="responsive"
          alt="AMP"></amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- servicos gerais -->
  <!-- ======================================================================= -->
  <div class="row">

  <?php
  $i = 0;
  $result = $obj_site->select("tb_dicas");
  if (mysql_num_rows($result) > 0) {
      while ($row = mysql_fetch_array($result)) {
          ?>

          <div class="col-6 dicas top20">
              <div class="card">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php echo $row['url_amigavel'] ?>">
                      <amp-img
                              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>"
                              width="150"
                              height="100"
                              layout="responsive"
                              alt="<?php echo Util::imprime($row[titulo]) ?>">
                      </amp-img>
                      <div class="card-body bottom10 text-center">
                          <h6 class="card-text"><?php Util::imprime($row[titulo]); ?></h6>
                      </div>
                      </a>
              </div>
          </div>

          <?php
          if ($i == 1) {
              echo '<div class="clearfix"></div>';
              $i = 0;
          } else {
              $i++;
          }
      }
  }
  ?>

  </div>
  <!-- ======================================================================= -->
  <!-- servicos gerais -->
  <!-- ======================================================================= -->



  <div class="row top25">
    <div class="col-12">
      <a class="btn col-12 padding0 btn-verde"
      on="tap:my-lightbox444"
      role="a"
      tabindex="0">
      <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
    </a>
    </div>


  </div>



  </div>



  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/veja.php") ?>
  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->




  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
