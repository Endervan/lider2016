<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url = $_GET[get1];

if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_especificacoes", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/mobile/especificacoes/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center no-repeat;
            background-size: 100% 128px;
        }
    </style>

    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-image-lightbox"
            src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>


</head>

<body class="bg-interna">

<?php
$voltar_para = $obj_site->url_amigavel($config[menu_4]);; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>


<!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row clearfix ">
      <div class="">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/bg-escpecificacoes.jpg"
          width="380"
          height="160"
          layout="responsive"
          alt="AMP"></amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->




  


  <div class="col-12 top20">
        <a class="btn btn-block padding0 btn_cinza_escuro"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
           <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA 
        </a>
    </div>



<div class="col-12 empresa_geral top15">
        <h4 class="open"><?php echo Util::imprime($dados_dentro[titulo]) ?></h4>
    </div>



    <div class="col-12 padding0 top10">

       
<amp-carousel id="carousel-with-preview"
              width="360"
              height="280"
              layout="responsive"
              type="slides"
              autoplay
              delay="8000"
>

    

            <amp-img
                    on="tap:lightbox1"
                    role="button"
                    tabindex="0"

                    src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($dados_dentro[imagem]) ?>"
                    layout="responsive"
                    width="360"
                    height="280"
                    alt="<?php echo Util::imprime($dados_dentro[titulo]) ?>">

            </amp-img>
            

</amp-carousel>

<amp-image-lightbox id="lightbox1" layout="nodisplay"></amp-image-lightbox>

</div>




    <div class="col-12 top20">
        <div><p><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
    </div>

    <div class="col-12 top20 bottom20">
        <a class="btn btn-block padding0 btn_cinza_escuro"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
           <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
        </a>
    </div>







<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->
<?php require_once("../includes/veja.php") ?>
<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
