<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("./includes/head.php"); ?>

    <style amp-custom>
        <?php require_once("./css/geral.css"); ?>
        <?php require_once("./css/topo_rodape.css"); ?>
        <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php
        $result = $obj_site->select("tb_banners", "and tipo_banner = 3 order by rand() limit 1");
        if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
                .bg-interna {
                    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row['imagem'] ?>) top 40px center no-repeat;
                    -webkit-background-size: 100%;
                    -moz-background-size: 100%;
                    -o-background-size: 100%;
                    background-size: 100%;
                }
            <?php
            }
        }
        
        ?>

        
    </style>


</head>


<body class="bg-interna">

<div class="row bg-branco">
    <div class="col-12 text-center topo top5">
    <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" width="240" height="80" layout="" ></amp-img>
    </div>
</div>


<div class=" row font-index text-center">
    <div class="col-12 padding0 top30">
        
        <!-- <h4 class="text-center">TRATAMENTO E ISOLAMENTO ACÚSTICO</h4> -->
        
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_home.png" alt="Home" height="1"
                 width="144"></amp-img>
    </div>
</div>

<!-- ======================================================================= -->
<!--  MENU -->
<!-- ======================================================================= -->
<div class="row">

        <div class="col-4 top30">
            <div class="">
                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa">
                    <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-empresa.png" alt="Home" width="95" height="95" layout="" ></amp-img>
                </a>
            </div>   
        </div>


        <div class="col-4 top30">
            <div class="">
                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
                    <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-vacinas.png" alt="Home" width="95" height="95" layout="" ></amp-img>
                </a>
            </div>  
        </div>


        <div class="col-4 top30">
            <div class="">
                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/especificacoes">
                    <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-especificacoes.png" alt="Home" width="95" height="95" layout="" ></amp-img>
                </a>
            </div>  
        </div>

    




    <div class="col-4 top30">
        <div class="">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/dicas">
            <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-dicas.png" alt="Home" width="95" height="95" layout="" ></amp-img>
            </a>
        </div>  
    </div>
   

    <div class="col-4 top30">
        <div class="">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/contato">
                <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-contato.png" alt="Home" width="95" height="95" layout="" ></amp-img>
            </a>
        </div>     
    </div>
    

    <div class="col-4 top30">
        <div class="">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/onde-estamos">
            <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-onde-estamos.png" alt="Home" width="95" height="95" layout="" ></amp-img>
            </a>
        </div>  
    </div>


</div>
<!-- ======================================================================= -->
<!--  MENU -->
<!-- ======================================================================= -->



<?php require_once("./includes/rodape.php") ?>


</body>


</html>