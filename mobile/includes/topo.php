<div class="row bg_topo">

  <div class="col-2 " style="margin-top:28px;">
    <?php
    if(empty($voltar_para)){
      $link_topo = Util::caminho_projeto()."/mobile/";
    }else{
      $link_topo = Util::caminho_projeto()."/mobile/".$voltar_para;
    }
    ?>
    <a href="<?php echo $link_topo  ?>"><i class="fa fa-arrow-left fa-2x btn-topo" aria-hidden="true"></i></a>
  </div>

  <div class="col-8 top5 topo">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" width="240" height="80" ></amp-img>
    </a>
  </div>

  <div class="col-2 text-right"  style="margin-top:28px;">
    <button on="tap:sidebar.toggle" class="ampstart-btn caps m2 btn-topo"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></button>
  </div>
</div>



<amp-sidebar id="sidebar" layout="nodisplay" side="left" class="menu-mobile-principal">
  <ul class="menu-mobile">
    
    
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile"> <i class="fa fa-angle-right" aria-hidden="true"></i> HOME</a></li>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa"> <i class="fa fa-angle-right" aria-hidden="true"></i> EMPRESA</a></li>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos"> <i class="fa fa-angle-right" aria-hidden="true"></i> PRODUTOS</a></li>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/especificacoes"> <i class="fa fa-angle-right" aria-hidden="true"></i> ESPECIFICAÇÕES</a></li>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas"> <i class="fa fa-angle-right" aria-hidden="true"></i> DICAS</a></li>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/contato"> <i class="fa fa-angle-right" aria-hidden="true"></i> CONTATO</a></li>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/onde-estamos"> <i class="fa fa-angle-right" aria-hidden="true"></i> ONDE ESTAMOS</a></li>
     

  </ul>
</amp-sidebar>


<div class="col-6 top15">
    <a class="btn btn-block btn-verde"
        on="tap:my-lightbox444"
        role="buttom"
        tabindex="0">
        <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
    </a>
</div>

 
<div class="col-6 top15 bottom10">
  <a  class="btn btn-block btn-default"
    on="tap:my-lightbox10"
    role="a"
    tabindex="0">
    CATEGORIAS <i class="fa fa-caret-down left5" aria-hidden="true"></i>
  </a>

  <amp-lightbox id="my-lightbox10" layout="nodisplay">
    <div class="lightbox" role="a" on="tap:my-lightbox10.close" tabindex="0">
      <?php require_once("../includes/menu_produtos.php") ?>
    </div>
  </amp-lightbox> 

</div>
