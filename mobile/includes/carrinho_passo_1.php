<?php
  
# ==============================================================  #
# VERIFICO SE E PARA VERIFICAR A TAXA DE ENTREGA
# ==============================================================  #
if(isset($_GET[cep])):

  $_SESSION[local_entrega] = $obj_carrinho->verifica_local_entrega($_GET[cep]);

  if($_SESSION[local_entrega] == false){
    unset($_SESSION[dados_entrega]);
    $efetua_entrega = 'nao';
  }else{
    $_SESSION[dados_entrega] = $obj_carrinho->busca_endereco($_GET[cep]);
  }
endif;


# ==============================================================  #
# VERIFICO SE E PARA ATUALIZAR O CARRINHO
# ==============================================================  #
if(isset($_GET[btn_atualizar]) or isset($_GET[bairro])):

  $obj_carrinho->atualiza_itens($_GET['qtd']);
  //$_SESSION[id_cidade] = $_GET[cidade];

endif;


# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if(isset($_GET[action])):

   $action = ($_GET[action]);
   $id = ($_GET[id]);
 
   //  ESCOLHO A OPCAO
   switch($action):
 
     case 'del':
     $obj_carrinho->del_item($id);
     break;
 
   endswitch;
 
 endif;


 
# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if(isset($_GET[action]) and $_GET[action] == 'add'):

  $obj_carrinho->add_item($_GET['idproduto']);

endif;


?>








    <div class="row">
      <div class="col-12">
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
        <ul class="nav  top15">
          <li class="input100">
            <button type="button" class="btn btn-lg btn-block btn_1"><i class="fa fa-shopping-cart right20" aria-hidden="true"></i> MEU CARRINHO</button>      
          </li>
        </ul>
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
      </div>

    </div>


    <form method="get" action="#" target="_top">
      <div class="row">

        <!--  ==============================================================  -->
        <!-- CARRINHO-->
        <!--  ==============================================================  -->
        <div class="col-12 tb-lista-itens top10">


          <?php if(count($_SESSION[produtos]) == 0): ?>

            <div class="alert alert-danger text-center top10">
              <h1 class=''>Nenhum produto foi adicionado ao carrinho.<br></h1>
            </div>

            <div class="text-center top10 bottom10">
              <a class="btn btn_orcamento btn_verde" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" >CONTINUAR COMPRANDO</a>
            </div>


          </div>

        <?php else: ?>


          <table class="table wid">
            <tbody>

            


              <?php foreach($_SESSION[produtos] as $key=>$dado):  ?>

                <tr>

                  <td>
                    <amp-img
                    height="30" width="30"
                    src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dado[imagem]); ?>" alt="<?php echo Util::imprime($dado[titulo]) ?>">
                  </amp-img>
                </td>
                <td align="left">
                  <p class="text-uppercase"><?php Util::imprime($dado[titulo]) ?></p>
                  <p class="cor_preco_carrinho">R$ <?php echo Util::formata_moeda($dado[preco]) ?></p>
                </td>

                
                <td class="text-center">
                  <input type="number" min="1" class="input-lista-prod-orcamentos" name="qtd[]" value="<?php echo $dado[qtd] ?>" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                  <input name="idproduto[]" type="hidden" value="<?php echo $_SESSION[produtos][idproduto]; ?>"  />
                </td>
                <td class="text-center">
                  <a href="?id=<?php echo ($key) ?>&action=<?php echo ("del") ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
                  <i class="far fa-trash-alt"></i>  
                  </a>
              </td>
            </tr>

            <?php $total += $dado[preco] * $dado[qtd]; ?>
          <?php endforeach; ?>






        </tbody>
      </table>

    <?php endif; ?>


    <?php if(count($_SESSION[produtos]) > 0): ?>
      <div class="col-12 text-right">
        <input type="hidden" name="bairro" value="<?php echo $_SESSION[id_bairro_entrega]; ?>">
        <input class="btn btn_2" type="submit" name="btn_atualizar" id="btn_atualizar" value="ATUALIZAR CARRINHO"  />
      </div>
    <?php endif; ?>



    <!--  ==============================================================  -->
    <!-- CARRINHO-->
    <!--  ==============================================================  -->






    <div class="col-12 top15">
      <h1 class=" text-center linha-topo-bottom">LOCAL DE ENTREGA </h1>
    </div>



    <div class="col-6 ">
      <p class="top5"><strong>INFORME SEU CEP</strong></p>
    </div> 

    <div class="col-4">
      <div class="form-group ">
          <input type="text" maxlength="8" name="cep" class="form-control fundo-form1 input-lg btn btn_2 input100" placeholder="CEP" value="<?php echo $_SESSION[dados_entrega][cep] ?>">
      </div>
    </div>

    <div class="col-1">
      <div class="form-group ">
        <input type="submit" name="btn_taxa_entrega" id="btn_taxa_entrega" class="btn btn_finalizar" value="ok"  />
      </div>
    </div>



  

    <div class="col-12 top10">


      <?php if($efetua_entrega == 'nao'): ?>

          <p class="top20 alert alert-danger">Infelizmente não efetuamos entrega em sua região.</p class="top20 alert alert-danger">

      <?php else: ?>

      <?php if(isset($_SESSION[produtos]) == 0 and !isset($_SESSION[local_entrega])): ?>
        <p class="top15 pull-right">
          TAXA DE ENTREGA: R$ 0,00</p>
        <?php else: ?>
          <p class="top15 pull-right">
            TAXA DE ENTREGA:
            <?php                              
              if ($_SESSION[local_entrega][valor] == 0) {
                echo "<span class='text-success'><b>GRÁTIS</b></span>";
              }else{
                echo '<span class="text-success"><b>R$ '.Util::formata_moeda($_SESSION[local_entrega][valor] ).'</b></span>';
              }
            ?>
          </p>
        <?php endif; ?>

      <?php endif; ?>


     
    </div>







    <div class="col-12 top15 text-right">
      <h1 class="linha-topo-bottom">VALOR TOTAL :
        <b>  R$ <?php echo Util::formata_moeda($total + $frete[valor]) ?></b>
      </h1>
    </div>

     <div class="col-6">
        <a class="btn btn_2 top20" href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
          COMPRAR MAIS
        </a>
      </div> 

      <div class="col-6 text-right">
        <?php if(!empty($_SESSION[dados_entrega][cep]) ): ?>
          <a class="btn btn_1 top20" href="<?php echo Util::caminho_projeto(); ?>/mobile/endereco-entrega">
            FINALIZAR PEDIDO
          </a>
        <?php endif; ?>
      </div>



    <!--  ==============================================================  -->
    <!-- CAL CARRINHO -->
    <!--  ==============================================================  -->
  </div>
</div>

</form>




<!--  ==============================================================  -->
<!--  PAGINA DO CARRINHO -->
<!--  ==============================================================  -->

