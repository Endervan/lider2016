<div class="row">

    <div class=" rodape  col-12 padding0">


        <div class="col-6">
            <a 
                on="tap:my-lightbox444"
                role="a"
                tabindex="0">
                <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
            </a>
        </div>


      

            
            <?php if(!empty($config[ddd2]) and !empty($config[telefone2]) ): ?>                      
                <div class="whatsappFixed">
                    <a href="https://api.whatsapp.com/send?phone=55<?php echo Util::trata_numero_whatsapp($config[ddd2].$config[telefone2]); ?>&text=Olá,%20gostaria%20de%20solicitar%20um%20orçamento.">
                    <amp-img
                                  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/whatsapp.png"
                                  width="150"
                                  height="150"
                                  layout="responsive"
                                  alt="<?php echo Util::imprime($row[titulo]) ?>">
                                </amp-img>
                    </a>
                </div>         
            <?php endif; ?>


        <div class="col-6 text-right">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
            <i class="fa fa-shopping-cart ml-1" aria-hidden="true"></i> ORÇAMENTO
            </a>

            </a>
        </div>

    </div>
</div>

<div class="row">


    <amp-lightbox id="my-lightbox444" layout="nodisplay">


        <div class="lightbox"
             role="button"
             on="tap:my-lightbox444.close"
             tabindex="0">

            <div class="col-12 bg_branco">
                <p class="text-center top5">Selecione o número desejado.</p>
                

                <div class="col-12 padding0 bg_branco_borde top10">
                    <h4 class="top5 col-8">
                        <a class=" numero"
                           href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
                            <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                        </a>
                    </h4>
                    <a class="btn btn-dark col-4 padding0"
                       href="tel:+55<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>">
                        CHAMAR
                    </a>
                </div>

                <?php if (!empty($config[telefone2])) : ?>
                    <div class="col-12 padding0 bg_branco_borde">
                        <h4 class="top5 col-8">
                            <a class="numero"
                               href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
                                <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                            </a>
                        </h4>
                        <a class="btn btn-dark col-4 padding0"
                           href="tel:+55<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>">
                            CHAMAR
                        </a>
                    </div>
                <?php endif; ?>



                <?php if (!empty($config[telefone3])): ?>
                    <div class="col-12 padding0 bg_branco_borde">
                        <h4 class="top5 col-8">
                            <a class=" numero"
                               href="tel:+55<?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>">
                                <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
                            </a>
                        </h4>
                        <a class="btn btn-dark col-4 padding0"
                           href="tel:+55<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>">
                            CHAMAR
                        </a>
                    </div>
                <?php endif; ?>


                <?php if (!empty($config[telefone4])): ?>
                    <div class="col-12 padding0 bg_branco_borde">
                        <h4 class="top5 col-8">
                            <a class=" numero"
                               href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
                                <?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>
                            </a>
                        </h4>
                        <a class="btn btn-dark col-4 padding0"
                           href="tel:+55<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>">
                            CHAMAR
                        </a>
                    </div>
                <?php endif; ?>


            </div>

        </div>
    </amp-lightbox>


</div>
