<div class="row">
    rodape.php

    <div class=" rodape  rodape_paginas col-12 padding0">


        <div class="col-6">
            <a
                    on="tap:my-lightbox444"
                    role="a"
                    tabindex="0">
                <i class="fa fa-phone mr-3" aria-hidden="true"></i>LIGAR AGORA
            </a>
        </div>

        <div class="col-6 text-right">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
                MEU ORÇAMENTO<i class="fa fa-shopping-cart ml-1" aria-hidden="true"></i>
            </a>

            </a>
        </div>

    </div>
</div>

<div class="row">


    <amp-lightbox id="my-lightbox444" layout="nodisplay">


        <div class="lightbox"
             role="button"
             on="tap:my-lightbox444.close"
             tabindex="0">

            <div class="col-12 bg_branco">
                <a class="btn btn_fechar bottom50">X</a>

                <div class="col-12 padding0 bg_branco_borde">
                    <h4 class="top5 col-8">
                        <a class=" numero"
                           href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
                            <?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>
                        </a>
                    </h4>
                    <a class="btn btn_verde col-4 padding0"
                       href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
                        CHAMAR
                    </a>
                </div>

                <?php if (!empty($config[telefone2])) : ?>
                    <div class="col-12 padding0 bg_branco_borde">
                        <h4 class="top5 col-8">
                            <a class="numero"
                               href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
                                <?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>
                            </a>
                        </h4>
                        <a class="btn btn_verde col-4 padding0"
                           href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
                            CHAMAR
                        </a>
                    </div>
                <?php endif; ?>



                <?php if (!empty($config[telefone3])): ?>
                    <div class="col-12 padding0 bg_branco_borde">
                        <h4 class="top5 col-8">
                            <a class=" numero"
                               href="tel:+55<?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>">
                                <?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>
                            </a>
                        </h4>
                        <a class="btn btn_verde col-4 padding0"
                           href="tel:+55<?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>">
                            CHAMAR
                        </a>
                    </div>
                <?php endif; ?>


                <?php if (!empty($config[telefone4])): ?>
                    <div class="col-12 padding0 bg_branco_borde">
                        <h4 class="top5 col-8">
                            <a class=" numero"
                               href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
                                <?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>
                            </a>
                        </h4>
                        <a class="btn btn_verde col-4 padding0"
                           href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
                            CHAMAR
                        </a>
                    </div>
                <?php endif; ?>


            </div>

        </div>
    </amp-lightbox>


</div>
