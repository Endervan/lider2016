 <?php
//error_reporting(0);

/*	================================================================================================	*/
/*	DEFINE O CAMINHO COMPLETO DO PROJETO ARMAZENANDO NUMA SESSAO	*/
/*	================================================================================================	*/
$pasta_projeto = "/clientes/lider2016";
//$pasta_projeto = "";



/*	================================================================================================	*/
/*	HABILITA O REDIRECIONAMENTO DO MOBILE, USE SIM OU NAO	*/
/*	================================================================================================	*/
define('MOBILE', 'SIM');


/*	================================================================================================	*/
/*	Redirecionamento 301	*/
/*	================================================================================================	*/
header( 'HTTP/1.1 301 Moved Permanently' );
//header( 'Location: http://www.seudominio.com.br/' );


/*	================================================================================================	*/
/*	CONFIGURACOES DO BANCO DE DADOS	*/
/*	================================================================================================	*/
$host_banco 	= "localhost";
$nome_banco 	= "liderportaseportoes";
$usuario_banco 	= "root";
$senha_banco 	= "";



/*	================================================================================================	*/
/*	DADOS SMTP GMAIL */
/*	================================================================================================	*/
define('PORTA', 587);
define('HOST', 'smtp.sendgrid.net');
define('USERNAME', 'apikey');
define('PASSWORD', '');
define('SMTP_SECURE', 'SSL');


/*	================================================================================================	*/
/*	SITEMAPS - deve colocar as páginas principais */
/*	================================================================================================	*/
$sitemaps[] = array('url' => "empresa");
$sitemaps[] = array('url' => "produtos");
$sitemaps[] = array('url' => "portfolios");
$sitemaps[] = array('url' => "dicas");
$sitemaps[] = array('url' => "fale-conosco");
$sitemaps[] = array('url' => "trabalhe-conosco");
$sitemaps[] = array('url' => "orcamento");


/*	================================================================================================	*/
/*	SITEMAPS TABELA - Informe as tabelas que deverão ser geradas os link para o sitemaps
/*	================================================================================================	*/
$sitemaps_tabela[] = array(
							'url' => "produto",
							'nome' => "tb_produtos",
						   );

$sitemaps_tabela[] = array(
							'url' => "dica",
							'nome' => "tb_dicas",
						   );

$sitemaps_tabela[] = array(
							'url' => "portfolio",
							'nome' => "tb_portfolios",
						   );























/*	================================================================================================	*/
/*	VARIAVEIS */
/*	================================================================================================	*/
define('PASTA_PROJETO', $pasta_projeto);
define('HOST_BANCO', $host_banco);
define('NOME_BANCO', $nome_banco);
define('USUARIO_BANCO', $usuario_banco);
define('SENHA_BANCO', $senha_banco);
$_SESSION['SITEMAPS'] = $sitemaps;
$_SESSION['SITEMAPS_TABELA'] = $sitemaps_tabela;



define('COPYRIGHT', "Copyright HomewebBrasil www.homewebbrasil.com.br");
define('EMAIL_GOOGLE_ANALYTICS', "");
define('SENHA_GOOGLE_ANALYTICS', "");
define('ID_GOOGLE_ANALYTICS', "");




?>
