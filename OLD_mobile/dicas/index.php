<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>

</head>

<body>
	<?php require_once('../includes/topo.php'); ?>

  

  


  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--- barra dicas descricao-->
  <!--  ==============================================================  --> 
  <div class="container">
    <div class="row descricao-empresa bottom25">
      <div class="col-xs-6 text-center top10  ">
        <h3> NOSSAS DICAS</h3>
      </div>

      <div class="col-xs-6 top10">
        <form action="<?php echo Util::caminho_projeto() ?>/mobile/dicas/" method="post">
          <div class="input-group ">
            <input type="text" class="form-control fundo-form" placeholder="PESQUISA DICAS" name="pesquisa_dicas">
            <span class="input-group-btn fundo-form">
              <button class="btn btn-default btn-pesquisa" type="submit">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              </button>
            </span>
          </div>
        </form>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - barra  dicas descricao-->
  <!--  ==============================================================  -->

  <!--  ==============================================================  -->
  <!--- dicas-->
  <!--  ==============================================================  --> 
  <div class="container bottom50">
    <div class="row">

      <?php 
      if (isset($_POST[pesquisa_dicas])) {
        $complemento = "AND titulo LIKE '%$_POST[pesquisa_dicas]%' ";
      }

      $result = $obj_site->select("tb_dicas", $complemento);
      if (mysql_num_rows($result) == 0) {
      ?> 
        <div class="col-xs-12  alert alert-danger top30" role="alert">
          Nenhuma dica encontrada.
        </div> 
      <?php
      }else{
        $i = 1;
        while($row = mysql_fetch_array($result)){
        ?>
            <div class="col-xs-6 text-center">
              <div class="thumbnail dicas-home">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 200, 181) ?>
                  <div class="borda-interna1"></div>
                </a>
                <div class="caption">
                  <p class="top20"><?php Util::imprime($row[titulo]); ?></p>
                  <a class="btn btn-transparente-home1 top20 bottom20" href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" role="button">SAIBA MAIS</a>
                </div>
              </div>
            </div>
        <?php 

          if($i ==2){
            echo '<div class="clearfix"></div>';
            $i = 1;
          }else{
            $i++;
          }

        }
      }
      ?>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- -  dicas-->
  <!--  ==============================================================  -->



  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>