<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html>

<head> 
	<?php require_once('.././includes/head.php'); ?>


  <!-- FlexSlider -->
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />

  <!-- usando carroucel 01 -->
  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 100,
        itemMargin: 1,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 100,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>



  <!-- usando carroucel 02 -->
  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel1').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 120,
        itemMargin: 1,
        asNavFor: '#slider1'
      });

      $('#slider1').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 120,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel1"
      });
    });
  </script>


</head>

<body>
	<?php require_once('../includes/topo.php'); ?>



	
  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->

  
  <!--  ==============================================================  -->
  <!-- CATEGORIAS PRODUTOS-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">
     <div class="col-xs-12 menu text-center">
      <div id="slider" class="flexslider">
        <ul class="slides slider-prod">
         <li class="categorias">
           <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
             <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-destaques.png" alt="TODAS">
             <h4><span>TODAS AS CATEGORIAS</span></h4>
           </a>
         </li>

        
        <?php 
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
              <li class="categorias">
               <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                 <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                 <h4><span><?php Util::imprime($row[titulo]); ?></span></h4>
               </a>
             </li>
            <?php 
            }
          }
          ?>
         <!-- items mirrored twice, total of 12 -->
       </ul>   
     </div>
   </div>

 </div>
</div>
<!--  ==============================================================  -->
<!-- CATEGORIAS PRODUTOS-->
<!--  ==============================================================  -->





<!--  ==============================================================  -->
<!-- IMAGEM CARROUEL TABS-->
<!--  ==============================================================  -->
<div class="container top20">
  <div class="row">
    <div class="col-xs-12 tabs">
      <div id="slider1" class="flexslider">
        <ul class="slides slider-prod">

          <?php 
          $result = $obj_site->select("tb_portfolios");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
              <li class="categorias <?php if(++$b == 1){ echo "active"; } ?> ">
               <a href="<?php echo Util::caminho_projeto() ?>/mobile/portfolio/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                 <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 120, 91) ?>
               </a>
             </li>
            <?php 
            }
          }
          ?>
         <!-- items mirrored twice, total of 12 -->
       </ul>   
     </div>
   </div>

 </div>
</div>
<!--  ==============================================================  -->
<!-- IMAGEM CARROUEL TABS-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!--TABS USANDO CAROUCEL-->
<!--  ==============================================================  -->
<div class="container top20 bottom20">
  <div class="row ">

    <!-- Tab panes -->
    <div class="tab-content">

      <?php 
      $result = $obj_site->select("tb_portfolios", "order by rand() limit 1");
      if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
        ?>
      <!-- CONTEUDO PORTIFOLIO 01-->
      <div role="tabpanel" class="tab-pane fade in active" id="home">

        <div class="container posicao1">
          <div class="row dica-dentro">
            <div class="col-xs-10 top40">
              <h1><?php Util::imprime($row[titulo]); ?></h1>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-7">
              <p class=""><?php Util::imprime($row[descricao]); ?></p>
            </div>

            <div class="col-xs-5  top10 triangulo-grande">
              <div class="imagem relative top80">    
                <a href="" title="">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 170, 135, array("class"=>"pull-right")) ?>               
                  <div class="borda-portifolio"></div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- CONTEUDO PORTIFOLIO 01-->
      <?php 
      }
    }
    ?>



      




    </div>
    <!-- Tab panes -->

  </div>
</div>
<!--  ==============================================================  -->
<!--TABS USANDO CAROUCEL-->
<!--  ==============================================================  -->




<?php require_once('../includes/rodape.php'); ?>

</body>

</html>