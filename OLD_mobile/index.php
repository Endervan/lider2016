<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 100,
        itemMargin: 1,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 230,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>

</head>

<body>
  <?php require_once('./includes/topo.php'); ?>



  <!-- ======================================================================= -->
  <!-- slider -->
  <!-- ======================================================================= -->  
  <div class="container">
    <div class="row slider-index">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
           if(mysql_num_rows($result) > 0){
            $i = 0;
             while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
             ?> 
                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
             <?php 
              $i++;
             }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <?php 
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
            ?>
                <div class="item <?php if($i == 0){ echo "active"; } ?>">
                  <img class="<?php echo $i ?>-slide" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                    <div class="container">
                      <div class="carousel-caption">

                          <h1><?php Util::imprime($imagem[legenda]); ?></h1>
                        
                          <?php if (!empty($imagem[url])) { ?>
                            <a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>
                          <?php } ?>
                          
                      </div>
                    </div>
                </div>
            <?php
              $i++;
            }
          }
          ?>


        </div>


            </div>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- slider -->
        <!-- ======================================================================= -->






<!-- ======================================================================= -->
<!-- categorias  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row slider-categorias">
    <div class="col-xs-12">
    <div id="slider" class="flexslider">
        <ul class="slides ">
         

        
        <?php 
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {

              //  busca a imagem do produto
                    $result1 = $obj_site->select("tb_produtos", "and imagem <> '' and id_categoriaproduto = $row[0] order by rand() limit 1");
                    if (mysql_num_rows($result1) > 0) {
                      while ($row1 = mysql_fetch_array($result1)) {
                        $row_imagem = $row1[imagem];
                      }
                    } 

                    //  conta a qt de produtos
                    $result1 = $obj_site->select("tb_produtos", "and id_categoriaproduto = '$row[0]'");
                    $total_prod = 0;
                    if (mysql_num_rows($result1) > 0) {
                      $total_prod = mysql_num_rows($result1);
                    } 

            ?>
              <li class="col-xs-6">
                <div class="pull-left">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row_imagem", 77, 115); ?>  
                </div>
                <div class="desc-categoria">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                    <p class="top20 text-center"><?php Util::imprime($total_prod ); ?> MODELO(S)</p>
                  </a>
                </div>
             </li>
            <?php 
            }
          }
          ?>
         
         <!-- items mirrored twice, total of 12 -->
       </ul>   
     </div>
   </div>

  </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- categorias  -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- titulo destaque  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    <div class="col-xs-8 titulo-destaque bottom20">
      <h6 class="text-right">PRODUTOS EM DESTAQUE</h6>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- titulo destaque  -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- produtos -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top20">

    

      <?php 
      $result = $obj_site->select("tb_produtos", "order by rand() limit 2");
      if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
        ?>
        <div class="col-xs-12 lista-produto">
          <div class="">
            <h1 class="bottom5"><?php Util::imprime($row[titulo]); ?></h1>

            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 170, 160, array("class"=>"pull-left right10 bottom10")) ?>
            </a>
            
            <p><?php Util::imprime($row[descricao], 300); ?></p>
            
            <a href="javascript:void(0);" class="btn btn-transparente" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                Adicionar ao orçamento
            </a>


          </div>
        </div>
        <?php 
        }
      }
      ?> 

    



  </div>
</div>
<!-- ======================================================================= -->
<!-- produtos -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- conheca mais -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row bg-conheca-lider top60">
    <div class="col-xs-6 col-xs-offset-6">
      <h2 class="text-right">CONHEÇA MAIS A</h2>
      <h2 class="text-right"><span>LÍDER PORTAS E PORTÕES</span></h2>
      <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa" title="">
        <p class="top10"><?php Util::imprime($dados[descricao]) ?></p>
      </a>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- conheca mais -->
<!-- ======================================================================= -->






<!-- ======================================================================= -->
<!-- titulo destaque  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    <div class="col-xs-8 col-xs-offset-4 titulo-destaque top30 bottom20">
      <h6>CONFIRA NOSSAS DICAS</h6>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- titulo destaque  -->
<!-- ======================================================================= -->



<!--  ==============================================================  -->
<!-- DICAS HOME -->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">

    
    <?php 
    

      $result = $obj_site->select("tb_dicas", "order by rand() limit 2");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
        ?>
            <div class="col-xs-6 text-center">
              <div class="thumbnail dicas-home">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 200, 181) ?>
                  <div class="borda-interna1"></div>
                </a>
                <div class="caption">
                  <p class="top20"><?php Util::imprime($row[titulo]); ?></p>
                  <a class="btn btn-transparente-home1 top20 bottom20" href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" role="button">SAIBA MAIS</a>
                </div>
              </div>
            </div>
        <?php 
        }
      }
      ?>
  


  </div>
</div>

<!--  ==============================================================  -->
<!-- DICAS HOME -->
<!--  ==============================================================  -->















    <?php require_once('./includes/rodape.php'); ?>





  </body>

  </html>