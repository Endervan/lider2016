<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>

</head>

<body>
  <?php require_once('../includes/topo.php'); ?>



  <!--  ==============================================================  -->
  <!--- bg fale-conosco-->
  <!--  ==============================================================  --> 
  <div class="container">
    <div class="row">
      <div class="bg-fale-conosco"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- -  bg fale-conosco-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!-- FORMULARIO CONTATOS E ENDERENCO-->
  <!--  ==============================================================  -->
  <div class="container ">
    <div class="row bottom20 subir-navs">
      <div class="col-xs-12 descricao-empresa">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="">
          <li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><img src=" <?php echo Util::caminho_projeto() ?>/imgs/icon-fale-conosco1.png" alt="">FALE CONOSCO</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><img src=" <?php echo Util::caminho_projeto() ?>/imgs/icon-trabalhe1.png" alt="">TRABALHE CONOSCO</a></li>
        </ul>
      </div>

      <div class="col-xs-12 bottom20 top50">
        <div class="media">
          <div class="media-left media-middle">
            <a href="#">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone.png" alt="">
            </a>
          </div>
          <div class="media-body media-middle">
            <h5 class="media-heading"><span>(61)</span>3552-1121<button type="button" class="btn btn-danger btn-vermelho" href="#">CHAMAR</button></h5>
            <h5 class="media-heading"><span>(61)</span>3552-1121<button type="button" class="btn btn-danger btn-vermelho" href="#">CHAMAR</button></h5>    
          </div>
        </div>

        <div class="media top20">
          <div class="media-left media-middle">
            <a href="#">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-enderenco.png" alt="">
            </a>
          </div>
          <div class="media-body media-middle">
           <p>2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF</p>
         </div>
       </div>        

     </div>



     <!-- Tab panes -->
     <div class="tab-content">

      <!-- fale conosco -->
      <div role="tabpanel" class="tab-pane fade in active" id="home">

        <!--  ==============================================================  -->
        <!-- BARRA ENVIE EMAIL-->
        <!--  ==============================================================  -->
        <div class="container">
          <div class="row">
            <div class="col-xs-8 titulo-destaque top30">
              <h6 class="text-right">ENVIE UMA E-MAIL</h6>
            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!-- BARRA ENVIE EMAIL-->
        <!--  ==============================================================  -->



        <!--  ==============================================================  -->
        <!-- BG DIREITA-->
        <!--  ==============================================================  -->
        <div class="container">
          <div class="row ">
            <div class="bg-fale"></div>
          </div>
        </div> 
        <!--  ==============================================================  -->
        <!-- BG DIREITA-->
        <!--  ==============================================================  -->


        <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
          <div class=" bottom25">

            <div class="col-xs-6">
              <!-- formulario orcamento -->
              <div class=" bottom80">  
                  <div class="col-xs-12 form-group  top10">
                    <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                    <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
                  </div>

                  <div class="col-xs-12 form-group  top10">
                    <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                    <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
                  </div>


                <div class="clearfix"></div>   
                  <div class="col-xs-12 form-group top10">
                    <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                    <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
                  </div>

                  <div class="col-xs-12 form-group">
                   <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                   <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="">
                 </div>


               <div class="clearfix"></div>    
               <div class="top10">
                <div class="col-xs-12 form-group top10">
                  <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                  <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="col-xs-12">

                <div class="pull-right  top30 bottom25">
                  <button type="submit" class="btn btn-formulario" name="btn_contato">
                    ENVIAR
                  </button>
                </div>
              </div>


            </div>
            <!-- formulario orcamento -->

          </div>

        </div>
      </form>



    </div>
    <!-- fale conosco -->



    <!-- trabalhe conosco -->
    <div role="tabpanel" class="tab-pane fade" id="profile">

      <!--  ==============================================================  -->
      <!-- BARRA TRABALHE CONOSCO-->
      <!--  ==============================================================  -->
      <div class="container">
        <div class="row">
          <div class="col-xs-8 titulo-destaque top30">
            <h6 class="text-right">TRABALHE CONOSCO</h6>
          </div>
        </div>
      </div>
      <!--  ==============================================================  -->
      <!-- BARRA TRABALHE CONOSCO-->
      <!--  ==============================================================  -->


      <!--  ==============================================================  -->
      <!-- BARRA HOME ESQUERDA-->
      <!--  ==============================================================  -->
      <div class="container">
        <div class="row ">
          <div class="bg-fale1"></div>
        </div>
      </div> 
      <!--  ==============================================================  -->
      <!-- BARRA HOME ESQUERDA-->
      <!--  ==============================================================  -->


      <form class="form-inline FormContato" role="form" method="post" enctype="multipart/form-data">
        <div class="top50 bottom25">

          <div class="col-xs-6">
            <!-- formulario orcamento -->
            <div class=" bottom80"> 
              <div class="clearfix"></div>   
                <div class="col-xs-12 form-group top10 ">
                  <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                  <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
                </div>

                <div class="col-xs-12 form-group top10 ">
                  <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                  <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
                </div>


              <div class="clearfix"></div>   
                <div class="col-xs-12 form-group top10">
                  <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                  <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
                </div>

                <div class="col-xs-12 form-group top10">
                  <label class="glyphicon glyphicon-book"> <span>Escolaridade</span></label>
                  <input type="text" name="escolaridade" class="form-control fundo-form1 input100" placeholder="">
                </div>

              <div class="clearfix"></div>>
               <div class="col-xs-12 form-group top10">
                <label class="glyphicon glyphicon-lock"> <span>Cargo</span></label>
                <input type="text" name="cargo" class="form-control fundo-form1 input100" placeholder="">
              </div>

              <div class="col-xs-12 form-group top10">
                <label class="glyphicon glyphicon-briefcase"> <span>Area</span></label>
                <input type="text" name="area" class="form-control fundo-form1 input100" placeholder="">
              </div>

            <div class="clearfix"></div> 
             <div class="col-xs-12 form-group top10">
              <label class="glyphicon glyphicon-home"> <span>Cidade</span></label>
              <input type="text" name="cidade" class="form-control fundo-form1 input100" placeholder="">
            </div>
            <div class="col-xs-12 form-group top10">
              <label class="glyphicon glyphicon-globe"> <span>Estado</span></label>
              <input type="text" name="estado" class="form-control fundo-form1 input100" placeholder="">
            </div>

          <div class="clearfix"></div>   
          <div class="top15">
            <div class="col-xs-12 form-group">
              <label class="glyphicon glyphicon-file"> <span>Currículo</span></label>
              <input type="file" name="curriculo" class="form-control fundo-form1 input100" placeholder="">
            </div>
          </div>


          <div class="clearfix"></div>    
          <div class="top10">
            <div class="col-xs-12 form-group">
              <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
              <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-12">
            <div class="pull-right  top30 bottom25">
              <button type="submit" class="btn btn-formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>


        </div>
        <!-- formulario orcamento -->

      </div>

    </div>
  </form>
</div>
<!-- trabalhe conosco -->

</div>
<!-- Tab panes -->

</div>
</div>
<!--  ==============================================================  -->
<!-- FORMULARIO CONTATOS E ENDERENCO-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- BARRA mapa-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <div class="col-xs-8 titulo-destaque top30">
      <h6 class="text-right">SAIBA COMO CHEGAR</h6>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- BARRA mapa-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<div class="container top10">
  <div class="row">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d245795.96134739765!2d-48.07832292335684!3d-15.721386976732337!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3d18df9ae275%3A0x738470e469754a24!2zQnJhc8OtbGlhLCBERg!5e0!3m2!1spt-BR!2sbr!4v1455731556267" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>


<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>











<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>



