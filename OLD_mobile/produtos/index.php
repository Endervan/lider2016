<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>


  <!-- FlexSlider -->
  <script defer src="<?php echo Util::caminho_projeto() ?>/lider2016/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 100,
        itemMargin: 1,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 150,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>


</head>

<body>
	<?php require_once('../includes/topo.php'); ?>



	
 <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->

  
  <!--  ==============================================================  -->
  <!-- CATEGORIAS PRODUTOS-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">
     <div class="col-xs-12 menu text-center">
      <div id="slider" class="flexslider">
        <ul class="slides slider-prod">
          


         <li class="categorias">
           <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
             <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-destaques.png" alt="TODAS">
             <h4><span>TODAS AS CATEGORIAS</span></h4>
           </a>
         </li>

        
        <?php 
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
              <li class="categorias">
               <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                 <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                 <h4><span><?php Util::imprime($row[titulo]); ?></span></h4>
               </a>
             </li>
            <?php 
            }
          }
          ?>

         
         <!-- items mirrored twice, total of 12 -->
       </ul>   
     </div>
   </div>

 </div>
</div>


<!--  ==============================================================  -->
<!-- CATEGORIAS PRODUTOS-->
<!--  ==============================================================  -->








<!--  ==============================================================  -->
<!--PRODUTOS-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row top15">
  

    <?php 

    //  VERIFICO SE FOI SELECIONADO A CATEGORIA
    if(isset($_GET[get1]) and $_GET[get1] != '' ){
        $idcategoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $_GET[get1]);
        $complemento = "AND id_categoriaproduto = '$idcategoria'";
    }


     //  VERIFICO SE FOI SELECIONADO A CATEGORIA
    if(isset($_POST[busca_topo]) and $_POST[busca_topo] != '' ){
        $complemento = "AND titulo LIKE '%$_POST[busca_topo]%' ";
    }


    $result = $obj_site->select("tb_produtos", $complemento);
    if (mysql_num_rows($result) == 0) {
      ?>
        <div class="col-xs-12  alert alert-danger" role="alert">
          <h1 class="">Nenhum produto encontrado.</h1> 
        </div>
      <?php
    }else{
      while ($row = mysql_fetch_array($result)) {
      ?>
      <div class="col-xs-12 lista-produto">
        <div class="">
          <h1 class="bottom5"><?php Util::imprime($row[titulo]); ?></h1>

          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 170, 160, array("class"=>"pull-left right10 bottom10")) ?>
          </a>
          
          <p><?php Util::imprime($row[descricao], 300); ?></p>
          
          <a href="javascript:void(0);" class="btn btn-transparente" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
              Adicionar ao orçamento
          </a>


        </div>
      </div>
      <?php 
      }
    }
    ?>

   


  </div>
</div>


<!--  ==============================================================  -->
<!--PRODUTOS-->
<!--  ==============================================================  -->




<?php require_once('../includes/rodape.php'); ?>

</body>

</html>