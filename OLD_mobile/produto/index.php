<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$id =$_GET[get1];


$complemento = "AND url_amigavel = '$id'";


$result = $obj_site->select("tb_produtos",$complemento);
if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/mobile/produtos/");
}

$dados_dentro = mysql_fetch_array($result);


// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>


  <!-- FlexSlider -->
  <script defer src="<?php echo Util::caminho_projeto() ?>/lider2016/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 100,
        itemMargin: 1,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 100,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>



</head>

<body>
	<?php require_once('../includes/topo.php'); ?>



	
  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->

  
  <!--  ==============================================================  -->
  <!-- CATEGORIAS PRODUTOS-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">
     <div class="col-xs-12 menu text-center">
      <div id="slider" class="flexslider">
        <ul class="slides slider-prod">
         <li class="categorias">
           <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
             <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-destaques.png" alt="TODAS">
             <h4><span>TODAS AS CATEGORIAS</span></h4>
           </a>
         </li>

        
        <?php 
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
              <li class="categorias">
               <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                 <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                 <h4><span><?php Util::imprime($row[titulo]); ?></span></h4>
               </a>
             </li>
            <?php 
            }
          }
          ?>
         
         <!-- items mirrored twice, total of 12 -->
       </ul>   
     </div>
   </div>

 </div>
</div>


<!--  ==============================================================  -->
<!-- CATEGORIAS PRODUTOS-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- DESCRICAO DICAS DENTRO -->
<!--  ==============================================================  -->
<div class="container posicao">
  <div class="row dica-dentro">
    <div class="col-xs-12 top40">
      <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-7">
      <p class=" top20 bottom50"><?php Util::imprime($dados_dentro[descricao]); ?></p>
    </div>
  
    <div class="col-xs-5  top10 triangulo-grande">
              <div class="imagem relative top80">    
                <a href="" title="">
                  <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 170, 135, array("class"=>"pull-right")) ?>               
                  <div class="borda-portifolio"></div>
                </a>
              </div>
            </div>



    <!-- carrinho e formas de pagamento -->
    <div class="col-xs-12 top30 ">
      <div>
       <a class="btn btn-default btn-cinza" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
         <i class="fa fa-shopping-cart  "></i>ADICIONAR AO MEU ORÇAMENTO
       </a>
     </div>





     <div class="media">
      <div class="media-left media-middle">
        <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/atendimento.png" alt="">
      </div>
      <div class="media-body">
        <h5 class="top10">
          <?php Util::imprime($config[telefone1]); ?>  
            <?php if (!empty($config[telefone2])) { ?>
             / <?php Util::imprime($config[telefone2]); ?>
          <?php } ?>  
        </h5>   
      </div>
    </div>


    <div class="pull-left ">
     <button type="button" class="btn btn-default btn-cinza1 top20"><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-cartao.png" alt="">FORMAS DE PAGAMENTO:</button>
     <div class="top20 tamanho-imagem">
     
       <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/cartao01.png" alt="">
    
       <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/cartao02.png" alt="">
     
       <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/cartao03.png" alt="">
     
       <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/cartao04.png" alt="">
    
   </div>
 </div>

</div>
<!-- carrinho e formas de pagamento -->




</div>

</div>
<!--  ==============================================================  -->
<!-- DESCRICAO DICAS DENTRO -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- VEJA TAMBÉM-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <div class="col-xs-8 titulo-destaque top30">
      <h6 class="text-right">VEJA TAMBÉM</h6>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- VEJA TAMBÉM-->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- CARROUCEL PRODUTOS DENTRO -->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <div class="col-xs-12 top20 bottom30 slider-prod-dentro">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators navs-carroucel">
          <?php
          $result = $obj_site->select("tb_produtos", "and idproduto <> '$dados_dentro[0]' order by rand() limit 4 ");
           if(mysql_num_rows($result) > 0){
            $i = 0;
             while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
             ?> 
                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
             <?php 
              $i++;
             }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          
          <?php 
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
            ?>  
                <div class="item <?php if($i == 0){ echo "active"; } ?>">
                  
                   <div class="col-xs-12 lista-produto">
                      <div class="">
                        <h1 class="bottom5"><?php Util::imprime($imagem[titulo]); ?></h1>
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($imagem[url_amigavel]); ?>" title="<?php Util::imprime($imagem[titulo]); ?>">
                          <?php $obj_site->redimensiona_imagem("../uploads/$imagem[imagem]", 170, 170, array("class"=>"pull-left right10", "alt"=>"$imagem[titulo]")); ?>
                        </a>
                        <p><?php Util::imprime($imagem[descricao], 300); ?></p>
                          
                        <a class="btn btn-transparente" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">ADICIONAR AO ORÇAMENTO</a>
                      </div>
                    </div>

                    <div class="carousel-caption"></div>


                  
                  <div class="carousel-caption lista-galeria"></div>
                </div>
            <?php
              $i++;
            }
          }
          ?>

        </div>

        <!-- Controls -->
        <a class="left carousel-control controles top20" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control controles top20" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>


<!--  ==============================================================  -->
<!-- CARROUCEL PRODUTOS DENTRO -->
<!--  ==============================================================  -->






<?php require_once('../includes/rodape.php'); ?>

</body>

</html>