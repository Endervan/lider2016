<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>

  <!-- Jumbo-Slider-2spaltig -->
  <script type="text/javascript">
    jQuery(document).ready(function($) {

      $('#myCarousel').carousel({
        interval: false,
      });

      $('#carousel-text').html($('#slide-content-0').html());


  // When the carousel slides, auto update the text
  $('#myCarousel').on('slid.bs.carousel', function (e) {
    var id = $('.item.active').data('slide-number');
    $('#carousel-text').html($('#slide-content-'+id).html());
  });
});
</script>
<!-- Jumbo-Slider-2spaltig -->


</head>

<body>
	<?php require_once('../includes/topo.php'); ?>



	<!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--- barra empresa descricao-->
<!--  ==============================================================  --> 
<div class="container">
  <div class="row descricao-empresa">
    <div class="col-xs-5 text-center top10">
      <h3>A EMPRESA</h3>
    </div>
    <div class="col-xs-7">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
        <p class="top5"><?php Util::imprime($row[descricao], 3000); ?></p>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- - barra  empresa descricao-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- DESCRICAO EMPRESA -->
<!--  ==============================================================  -->

<!-- ======================================================================= -->
<!-- conheca mais -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top30 bottom40">
    <div class="col-xs-12">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg-conheca-a-lider.jpg" alt="" class="pull-left right15">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
      <p><?php Util::imprime($row[descricao]); ?></p>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- conheca mais -->
<!-- ======================================================================= -->





<?php require_once('../includes/rodape.php'); ?>

</body>

</html>