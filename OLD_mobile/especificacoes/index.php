<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>

</head>

<body>
	<?php require_once('../includes/topo.php'); ?>



  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->

  <!--  ==============================================================  -->
  <!--- barra especificacoes -->
  <!--  ==============================================================  --> 
  <div class="container">
    <div class="row descricao-empresa">
      <div class="col-xs-6 text-center top10">
        <h3>ESPECIFICAÇÕES</h3>
      </div>
      <div class="col-xs-6">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
        <p class="top10"><?php Util::imprime($row[descricao], 3000); ?></p>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - barra  especificacoes -->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!-- descricoes especificacoes -->
  <!--  ==============================================================  -->  
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top20 bottom15">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
        <p><?php Util::imprime($row[descricao]); ?></p>
     </div>
   </div>
 </div>
 <!--  ==============================================================  -->
 <!-- descricoes especificacoes -->
 <!--  ==============================================================  -->



 <!--  ==============================================================  -->
 <!--- especificacoes-->
 <!--  ==============================================================  --> 
 <div class="container bottom50">
  <div class="row">



      <?php
      $result = $obj_site->select("tb_especificacoes");
      if (mysql_num_rows($result) > 0) {
        $i = 1;
        while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-6 text-center top15 ">
          <div class=" dicas-especificacoes">
              <a href="javascript:void(0);" title="<?php Util::imprime($row[titulo]); ?>" data-toggle="modal" data-target="#myModal<?php Util::imprime($row[0]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 210, 190, array("alt"=>"$row[titulo]")) ?>
                <div class="borda-interna1"></div>
                <h1 class="top10"><span><?php Util::imprime($row[titulo]); ?></span></h1>
              </a>
          </div>
        </div>

        
        <!-- Modal -->
          <div class="modal fade" id="myModal<?php Util::imprime($row[0]); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg1" role="document">
              <div class="modal-content modal-lg1">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" class="input100">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
              </div>
            </div>
          </div>


        <?php 
        }
      }
      ?>


    

    


  </div>
</div>
<!--  ==============================================================  -->
<!-- -  especificacoes-->
<!--  ==============================================================  -->



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>