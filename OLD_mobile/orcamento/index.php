<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
        break;
    }

}


?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>

</head>

<body>
  <?php require_once('../includes/topo.php'); ?>


<form class="form-inline FormContato" role="form" method="post">

  <?php
  //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome]))
  {
      $assunto ="ORÇAMENTO PELO SITE";
      //  CADASTRO OS PRODUTOS SOLICITADOS
      for($i=0; $i < count($_POST[qtd]); $i++)
      {
          $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

          $mensagem .= "
                      <tr>
                          <td><p>". $_POST[qtd][$i] ."</p></td>
                          <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                       </tr>
                      ";
      }



      if (count($_POST[categorias]) > 0) {
          foreach($_POST[categorias] as $cat){
              $desc_cat .= $cat . ' , ';
          }
      }








      //  ENVIANDO A MENSAGEM PARA O CLIENTE
      $texto_mensagem = "
                          O seguinte cliente fez uma solicitação pelo site. <br />


                          Nome: $_POST[nome] <br />
                          Email: $_POST[email] <br />
                          Telefone: $_POST[telefone] <br />
                          Celular: $_POST[celular] <br />
                          Bairro: $_POST[bairro] <br />
                          Cidade: $_POST[cidade] <br />
                          Estado: $_POST[estado] <br />
                          Complemento: $_POST[complemento] <br />
                          Cep: $_POST[cep] <br />
                          Receber orçamento: $_POST[receber_orcamento] <br />
                          Categorias desejadas: $desc_cat <br />
                          Como conheceu: $_POST[como_conheceu] <br />


                          Mensagem: <br />
                          ".nl2br($_POST[mensagem])." <br />


                          <br />
                          <h2> Produtos selecionados:</h2> <br />

                          <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                              <tr>
                                    <td><h4>QTD</h4></td>
                                    <td><h4>PRODUTO</h4></td>
                              </tr>

                              $mensagem

                          </table>


                      
                          ";

      Util::envia_email($config[email], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
      Util::envia_email($config[email_copia], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
      unset($_SESSION[solicitacoes_produtos]);
      unset($_SESSION[solicitacoes_servicos]);
      unset($_SESSION[piscinas_vinil]);
      Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

  }
  ?>


  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 21) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--- barra empresa descricao-->
  <!--  ==============================================================  --> 
  <div class="container">
    <div class="row descricao-empresa">
      <div class="col-xs-12 top10">
        <h3>MEU ORÇAMENTO</h3>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - barra  empresa descricao-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!-- FORMULARIO CONTATOS E ENDERENCO-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row bottom20">

      <div class="col-xs-12 bottom20 top20">
        <div class="media">
          <div class="media-left media-middle">
            <a href="#">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone.png" alt="">
            </a>
          </div>
          <div class="media-body media-middle">
            <h5 class="media-heading">
              <?php Util::imprime($config[telefone1]); ?>
              <a href="tel:+55<?php Util::imprime($config[telefone1]); ?>" class="btn btn-danger btn-vermelho">CHAMAR</a>
            </h5>

            <?php if (!empty($config[telefone2])) { ?>
               <h5 class="media-heading">
                <?php Util::imprime($config[telefone2]); ?>
                <a href="tel:+55<?php Util::imprime($config[telefone2]); ?>" class="btn btn-danger btn-vermelho">CHAMAR</a>
              </h5>
            <?php } ?>

            <?php if (!empty($config[telefone3])) { ?>
               <h5 class="media-heading">
                <?php Util::imprime($config[telefone3]); ?>
                <a href="tel:+55<?php Util::imprime($config[telefone3]); ?>" class="btn btn-danger btn-vermelho">CHAMAR</a>
              </h5>
            <?php } ?>

            <?php if (!empty($config[telefone4])) { ?>
               <h5 class="media-heading">
                <?php Util::imprime($config[telefone4]); ?>
                <a href="tel:+55<?php Util::imprime($config[telefone4]); ?>" class="btn btn-danger btn-vermelho">CHAMAR</a>
              </h5>
            <?php } ?>
          </div>
        </div>

        <div class="media top20">
          <div class="media-left media-middle">
            <a href="#">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-enderenco.png" alt="">
            </a>
          </div>
          <div class="media-body media-middle">
           <p><?php Util::imprime($config[endereco]); ?></p>
         </div>
       </div>        

     </div>

   </div>
 </div>
 <!--  ==============================================================  -->
 <!-- FORMULARIO CONTATOS E ENDERENCO-->
 <!--  ==============================================================  -->





 <!--  ==============================================================  -->
 <!-- carrinho -->
 <!--  ==============================================================  -->
 <div class="container">
  <div class="row top20">

    <div class="col-xs-12">
      <h2>MEU ORÇAMENTO (<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h2>
      <div>


        <?php
          if(count($_SESSION[solicitacoes_produtos]) > 0)
          {
          ?>
          <table class="table tb-lista-itens">
            <thead>
              <tr>
                <th>ITEM</th>
                <th>DESCRIÇÃO</th>
                <th class="text-center">QUANTIDADE</th>
                <th class="text-center">REMOVER</th>
              </tr>
            </thead>


            <tbody>

              
              <?php
              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
              {
                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                ?>
                <tr>
                  <td><img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" height="88" width="100" ></td>
                  <td><?php Util::imprime($row[titulo]) ?></td>
                  <td class="text-center">
                    <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                    <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                  </td>
                  <td class="text-center">
                    <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                      <i class="glyphicon glyphicon-remove link-excluir"></i>
                    </a>
                  </td>
                </tr>
              <?php 
              }
              ?>


            </tbody>


          </table>
        <?php 
        }
        ?>



      </div>
      





      <div class=" top15">

        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="Continuar orçando" class="btn btn-cinza">
          Continuar orçando
        </a>
      </div>

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- carrinho  -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- CONFIRME SEUS DADOS  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    <div class="col-xs-7 titulo-destaque top30 bottom20">
      <h6 class="text-right">CONFIRME SEUS DADOS</h6>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- CONFIRME SEUS DADOS  -->
<!-- ======================================================================= -->





<!--  ==============================================================  -->
<!-- formulario -->
<!--  ==============================================================  -->
<div class="container">
  <div class="row top20 bottom20">

      


        <div class="titulo-form-orcamento">

          <!-- form fale conosco -->
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
            <input type="text" name="nome" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
            <input type="text" name="email" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 top5 form-group">
            <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
            <input type="text" name="telefone" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 top5 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Estado</span></label>
            <input type="text" name="estado" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 top5 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Cidade</span></label>
            <input type="text" name="cidade" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 top5 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Bairro</span></label>
            <input type="text" name="bairro" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 top5 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Complemento</span></label>
            <input type="text" name="complemento" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 top5 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Cep</span></label>
            <input type="text" name="cep" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-12 top5 form-group">
            <label class="glyphicon glyphicon-pencil"> <span>Mensagem</span></label>
            <textarea name="mensagem" id="" cols="30" rows="10" class="form-control input100"></textarea>
          </div>

         
          <!-- form fale conosco -->
        </div>




      <!-- perguntas frequentes -->
      <div class="col-xs-12 top30">
        <p>Tem interesse em receber orçamento de outros produtos:</p>

        <l<label class="radio-inline">
                <input type="radio" name="receber_orcamento" value="SIM"> SIM
              </label>
              <label class="radio-inline">
                <input type="radio" name="receber_orcamento"  value="NÃO"> NÃO
              </label>
        <p class="top25">Se sim, assinale abaixo os produtos</p>

        <?php
              $result = $obj_site->select("tb_categorias_produtos", "LIMIT 4");
              if(mysql_num_rows($result) > 0)
              {
                while($row = mysql_fetch_array($result))
                {
                ?>
                <div class="checkbox col-xs-12">
                  <label>
                    <input type="checkbox" name="categorias[]" value="<?php Util::imprime($row[titulo]) ?>">
                    <?php Util::imprime($row[titulo]) ?>
                  </label>
                </div>
                <?php
                }
            }
            ?>

        <p class="top20">Como você conheceu o nosso site?</p>

      </div>

      <div class=" col-xs-12 top10">
        <select class="form-control" name="como_conheceu">
          <option value="">Selecione</option>
          <option>GOOGLE</option>
          <option>INTERNET</option>
          <option>JORNAIS</option>
          <option>REVISTAS</option>
          <option>REDE SOCIAIS</option>
          <option>OUTROS</option>
        </select>
      </div>       

    <!-- perguntas frequentes -->


     <div class="clearfix"></div>

          
          <div class="col-xs-12 text-right top5 bottom20">
            <button type="submit" class="btn btn-default btn-cinza">ENVIAR</button>
          </div>


    
  

  </div>
</div>
<!--  ==============================================================  -->
<!-- formulario -->
<!--  ==============================================================  -->

</form>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>



<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'O telefone %s não é válido'
          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
    
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>











