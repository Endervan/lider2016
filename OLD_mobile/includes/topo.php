<div class="container topo">
  <div class="row bg-topo">

    <!--  ==============================================================  -->
    <!-- logo -->
    <!--  ==============================================================  -->
    <div class="col-xs-4 top10">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
      </a>
    </div> 

    <div class="col-xs-8 text-right">
      

      <div class="text-right">
              <div class="pull-right text-center topo-telefone-2">
                <?php Util::imprime($config[telefone1]); ?><br>
                <a href="tel:+55<?php Util::imprime($config[telefone1]); ?>" class="btn btn-topo-telefone" >CHAMAR</a>
              </div>

              <?php if (!empty($config[telefone2])): ?>
                  <div class="pull-right text-center topo-telefone-2 ">
                     <?php Util::imprime($config[telefone2]); ?> <br>
                    <a href="tel:+55 <?php Util::imprime($config[telefone2]); ?>" class="btn btn-topo-telefone">CHAMAR</a>
                  </div>
              <?php endif ?>
      </div>

      
      <!-- menu  -->
      <div class=" dropdown top5 pull-right right5">
        <a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-menu">
          <i class="fa fa-bars right35"></i> 
          NOSSO MENU
          <i class="fa fa-chevron-down"></i>
        </a>
        <ul class="dropdown-menu sub-menu" aria-labelledby="dLabel">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/especificacoes">ESPECIFICAÇÕES</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</a></li>
        </ul>
      </div>
      <!-- menu  -->        

    </div>

    <!--  ==============================================================  -->
    <!-- logo -->
    <!--  ==============================================================  -->



    <!-- ======================================================================= -->
    <!-- ligar  -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 topo-telefone padding0">
              

              <!-- botao pesquisa -->
              <div class=" dropdown top5 pull-left right5">
                

                    <form class="" role="search" action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
                      <div class="form-group col-xs-8 pull-left">
                        <input type="text" class="form-control busca-topo" placeholder="O que está procurando?" name="busca_topo">
                      </div>
                      <button type="submit" class="btn btn-azul-claro2 btn-busca-topo">Buscar</button>
                    </form>

                </div>

                <!-- botao pesquisas -->

                <div class="linha-divisoria pull-left"></div>


                <!-- ======================================================================= -->
                <!-- botao carrinho de compra -->
                <!-- ======================================================================= -->
                <div class=" dropdown top5 pull-right right5">
                  <a href="#" class="dropdown-toggle btn btn-azul-claro" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-shopping-cart right10"></i>
                    <span class="caret"></span></a>

                    <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">
                                       <?php
                                      
                                        if(count($_SESSION[solicitacoes_produtos]) > 0)
                                        {
                                            echo '<h1 class="bottom20">MEUS PRODUTOS('. count($_SESSION[solicitacoes_produtos]) .')</h1>';

                                            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                                            {
                                                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                                                ?>
                                                <div class="lista-itens-carrinho col-xs-12">
                                                    <div class="col-xs-2">
                                                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">    
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <h1><?php Util::imprime($row[titulo]) ?></h1>
                                                    </div>
                                                    <div class="col-xs-1 top20">
                                                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> 
                                                          X
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php  
                                            }
                                        }
                                      
                                        ?>

                                        <?php
                                        
                                        if(count($_SESSION[solicitacoes_servicos]) > 0)
                                        {

                                          echo '<h6 class="bottom20">MEUS SERVIÇOS('. count($_SESSION[solicitacoes_servicos]) .')</h6>';

                                            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                                            {
                                                $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                                                ?>
                                                <div class="lista-itens-carrinho col-xs-12">
                                                    <div class="col-xs-2">
                                                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">    
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <h1><?php Util::imprime($row[titulo]) ?></h1>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> 
                                                          X
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php  
                                            }
                                        }
                                      
                                        ?>

                                        <div class="text-right bottom20">
                                            <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" title="Finalizar" class="btn btn-primary" >
                                                FINALIZAR
                                            </a>
                                        </div>
                                    </div>
                  </div>
                  <!-- ======================================================================= -->
                  <!-- botao carrinho de compra -->
                  <!-- ======================================================================= -->





        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- ligar  -->
    <!-- ======================================================================= -->







  </div>
</div>


