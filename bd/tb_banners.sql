-- phpMyAdmin SQL Dump
-- version 4.4.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11-Abr-2016 às 11:43
-- Versão do servidor: 5.6.25
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alutec`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(8, 'FACHADAS EM PELE DE VIDRO 01', '1104201610541225362354.jpg', 'SIM', NULL, '1', 'index-banner-1', '/produtos', '01 Fachadas em Pele de Vidro são muito utilizadas em empreendi mentos comerciais e residenciais com grande estilo. Este produ to apresenta beleza, requinte, durabilidade e quando utilizado com vidro refletivo causa grande conforto térmico e, em muitas vez', NULL),
(9, 'FACHADAS EM PELE DE VIDRO 02', '1104201610551384998946.jpg', 'SIM', NULL, '1', 'inde-banner-2', '', '02 Fachadas em Pele de Vidro são muito utilizadas em empreendi mentos comerciais e residenciais com grande estilo. Este produ to apresenta beleza, requinte, durabilidade e quando utilizado com vidro refletivo causa grande conforto térmico e, em muitas vez', NULL),
(10, 'FACHADAS EM PELE DE VIDRO 03', '1104201610561121215735.jpg', 'SIM', NULL, '1', 'fachadas-em-pele-de-vidro-03', '/produtos', '01 Fachadas em Pele de Vidro são muito utilizadas em empreendi mentos comerciais e residenciais com grande estilo. Este produ to apresenta beleza, requinte, durabilidade e quando utilizado com vidro refletivo causa grande conforto térmico e, em muitas vez', NULL),
(11, 'FACHADAS EM PELE DE VIDRO 01', '1104201611341201506402.jpg', 'SIM', NULL, '2', 'fachadas-em-pele-de-vidro-01', 'produtos', '01 Fachadas em Pele de Vidro são muito utilizadas em empreendi mentos comerciais e residenciais com grande estilo. Este produ to', NULL),
(12, 'FACHADAS EM PELE DE VIDRO 02', '1104201611341351733189.jpg', 'SIM', NULL, '2', 'fachadas-em-pele-de-vidro-02', '', '02 Fachadas em Pele de Vidro são muito utilizadas em empreendi mentos comerciais e residenciais com grande estilo. Este produ to', NULL),
(13, 'FACHADAS EM PELE DE VIDRO 03', '1104201611351172586434.jpg', 'SIM', NULL, '2', 'fachadas-em-pele-de-vidro-03', '/mobile/produtos', '03 Fachadas em Pele de Vidro são muito utilizadas em empreendi mentos comerciais e residenciais com grande estilo. Este produ to', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
