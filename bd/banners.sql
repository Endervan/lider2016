-- phpMyAdmin SQL Dump
-- version 4.4.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26-Mar-2016 às 12:28
-- Versão do servidor: 5.6.25
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lider2016`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(41, 'Banner 1', '2603201611051338164485..jpg', 'SIM', NULL, '1', 'banner-1', '/produtos', 'Segurança para sua família', NULL),
(42, 'Banner 2', '2603201611031157250473..jpg', 'SIM', NULL, '1', 'banner-2', '/orcamento', 'Segurança acima de tudo', NULL),
(43, 'Banner 3', '2603201611041267028329..jpg', 'SIM', NULL, '1', 'banner-3', '/produtos', 'Segurança para sua familia 2', NULL),
(44, 'Mobile 1', '2603201611111310566045..jpg', 'SIM', NULL, '2', 'mobile-1', '/produtos', 'Segurança para sua familia', NULL),
(45, 'Mobile 2', '2603201611121171444439..jpg', 'SIM', NULL, '2', 'mobile-2', '/produtos', 'Segurança para sua familia', NULL),
(47, 'Mobile 3', '2603201611131263852710..jpg', 'SIM', NULL, '2', 'mobile-3', '/produtos', 'Segurança para sua familia', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE IF NOT EXISTS `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`) VALUES
(0, 'Empresa', '2603201611341198403098..jpg', NULL, 'empresa', 'SIM'),
(1, 'Produtos', '2603201611351214184685..jpg', NULL, 'produtos', 'SIM'),
(2, 'Produtos Dentro', '2603201611351362562793..jpg', NULL, 'produtos-dentro', 'SIM'),
(3, 'Dicas', '2603201611361237256647..jpg', NULL, 'dicas', 'SIM'),
(4, 'Dicas Dentro', '2603201611361297839046..jpg', NULL, 'dicas-dentro', 'SIM'),
(5, 'Especificações', '2603201611361382970806..jpg', NULL, 'especificacoes', 'SIM'),
(6, 'Portfólios', '2603201611361229433866..jpg', NULL, 'portfolio', 'SIM'),
(7, 'Portfólio Dentro', '2603201611361211056193..jpg', NULL, 'portfolio-dentro', 'SIM'),
(8, 'Fale Conosco', '2603201611371202694375..jpg', NULL, 'fale-conosco', 'SIM'),
(9, 'Trabalhe Conosco', '2603201611381333385986..jpg', NULL, 'trabalhe-conosco', 'SIM'),
(10, 'Mobile Empresa', '2603201612121170551576.jpg', NULL, 'mobile-empresa', 'SIM'),
(11, 'Mobile Produtos', '2603201612171233200223.jpg', NULL, 'mobile-produtos', 'SIM'),
(12, 'Mobile Produtos Dentro', '2603201612171320561255.jpg', NULL, 'mobile-produtos-dentro', 'SIM'),
(13, 'Mobile Dicas', '2603201612111183973028.jpg', NULL, 'mobile-dicas', 'SIM'),
(14, 'Mobile Dicas Dentro', '2603201612091202893848.jpg', NULL, 'mobile-dicas-dentro', 'SIM'),
(15, 'Mobile Especificações', '2603201612131147879866.jpg', NULL, 'mobile-especificacoes', 'SIM'),
(16, 'Mobile Portfólio', '2603201612151276738505.jpg', NULL, 'mobile-portfolio', 'SIM'),
(17, 'Mobile Portfólio Dentro', '2603201612161191731379.jpg', NULL, 'mobile-portfolio-dentro', 'SIM'),
(18, 'Mobile Fale Conosco', '2603201612131112925565.jpg', NULL, 'mobile-fale-conosco', 'SIM'),
(19, 'Mobile Trabalhe Conosco', '2603201612171310297221.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM'),
(20, 'Orçamento', NULL, NULL, NULL, 'SIM'),
(21, 'Mobile Orçamento', '2603201612141252610745.jpg', NULL, 'mobile-orcamento', 'SIM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
