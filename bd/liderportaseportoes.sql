-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 07-Maio-2019 às 16:08
-- Versão do servidor: 5.5.60-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `liderportaseportoes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idavaliacaoproduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idbanner`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=57 ;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(41, 'PORTAS DE AÇO AUTOMATIZADAS', '0609201607315039444747..jpg', 'SIM', NULL, '1', 'portas-de-aco-automatizadas', 'http://liderportaseportoes.com.br/produtos/portas-de-aco', 'PORTAS DE AÇO AUTOMATIZADAS', NULL),
(42, 'PORTÕES DE AÇO ELETRÔNICOS', '0609201607294311875204..jpg', 'SIM', NULL, '1', 'portoes-de-aco-eletronicos', 'http://liderportaseportoes.com.br/produtos/portoes-eletronicos', 'PORTÕES DE AÇO ELETRÔNICOS', NULL),
(43, 'PORTAS DE AÇO VAZADAS', '1605201603462152076053..jpg', 'SIM', NULL, '1', 'portas-de-aco-vazadas', 'http://liderportaseportoes.com.br/produto/portas-de-aco-transvision', 'PORTAS DE AÇO VAZADAS', NULL),
(44, 'PORTAS DE AÇO AUTOMATIZADAS MOBILE', '0609201608463923456652..jpg', 'SIM', NULL, '2', 'portas-de-aco-automatizadas-mobile', 'http://liderportaseportoes.com.br/mobile/produtos/portas-de-aco', 'Portas de Aço Automatizadas', NULL),
(45, 'Portas de aço Transvision - Mobile', '0609201608553349226154..jpg', 'SIM', NULL, '2', 'portas-de-aco-transvision--mobile', 'http://liderportaseportoes.com.br/mobile/produto/portas-de-aco-transvision', 'Portas de Aço Transvision', NULL),
(47, 'PORTÕES DE AÇO MOBILE', '0609201608453605774996..jpg', 'SIM', NULL, '2', 'portoes-de-aco-mobile', 'http://liderportaseportoes.com.br/mobile/produtos/portoes-eletronicos', 'Portões de Aço Eletrônicos', NULL),
(48, 'Grades e Alambrados', '1205201605093921494595..jpg', 'NAO', NULL, '1', 'grades-e-alambrados', 'http://liderportaseportoes.com.br/produtos/alambrados-grades', 'Grades e Alambrados', NULL),
(49, 'Estruturas e Galpões Metálicos - site', '1605201604407870176426..jpg', 'NAO', NULL, '1', 'estruturas-e-galpoes-metalicos--site', 'http://liderportaseportoes.com.br/produtos/estruturas-metalicas', 'Estruturas e Galpões Metálicos', NULL),
(50, 'Estruturas e Galpões Metálicos - mobile', '1605201604455279586368..jpg', 'NAO', NULL, '2', 'estruturas-e-galpoes-metalicos--mobile', 'http://liderportaseportoes.com.br/mobile/produtos/estruturas-metalicas', 'Estruturas e Galpões Metálicos', NULL),
(52, 'PORTÕES DE ENROLAR RESIDENCIAIS', '0609201607184532530386..jpg', 'SIM', NULL, '1', 'portoes-de-enrolar-residenciais', 'http://liderportaseportoes.com.br/produto/portoes-de-enrolar-residenciais', 'PORTÕES DE ENROLAR RESIDENCIAIS', NULL),
(55, 'PORTAS DE AÇO TRANSVISION', '0609201608176001602580..jpg', 'SIM', NULL, '1', 'portas-de-aco-transvision', 'http://liderportaseportoes.com.br/produto/portas-de-aco-transvision', 'PORTAS DE AÇO TRANSVISION', NULL),
(56, 'PORTÕES DE ENROLAR INDUSTRIAIS', '0609201607576126302261..jpg', 'SIM', NULL, '1', 'portoes-de-enrolar-industriais', 'http://liderportaseportoes.com.br/produto/portoes-de-enrolar-industriais', 'PORTÕES DE ENROLAR INDUSTRIAIS', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE IF NOT EXISTS `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  PRIMARY KEY (`idbannerinterna`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`) VALUES
(0, 'Empresa', '0609201609004534989988.jpg', NULL, 'empresa', 'SIM'),
(1, 'Produtos', '0609201609044621951360.jpg', NULL, 'produtos', 'SIM'),
(2, 'Produtos Dentro', '0609201609079198114474.jpg', NULL, 'produtos-dentro', 'SIM'),
(3, 'Dicas', '0609201609066449978426.jpg', NULL, 'dicas', 'SIM'),
(4, 'Dicas Dentro', '0609201609065698595444.jpg', NULL, 'dicas-dentro', 'SIM'),
(5, 'Especificações', '0609201609106206865149.jpg', NULL, 'especificacoes', 'SIM'),
(6, 'Portfólios', '2603201611361229433866..jpg', NULL, 'portfolio', 'SIM'),
(7, 'Portfólio Dentro', '2603201611361211056193..jpg', NULL, 'portfolio-dentro', 'SIM'),
(8, 'Fale Conosco', '1605201605487016848321.jpg', NULL, 'fale-conosco', 'SIM'),
(9, 'Trabalhe Conosco', '1605201605503014174757.jpg', NULL, 'trabalhe-conosco', 'SIM'),
(10, 'Mobile Empresa', '1605201605245292027816.jpg', NULL, 'mobile-empresa', 'SIM'),
(11, 'Mobile Produtos', '1605201605202248470576.jpg', NULL, 'mobile-produtos', 'SIM'),
(12, 'Mobile Produtos Dentro', '1605201605215743273753.jpg', NULL, 'mobile-produtos-dentro', 'SIM'),
(13, 'Mobile Dicas', '1605201605262364498327.jpg', NULL, 'mobile-dicas', 'SIM'),
(14, 'Mobile Dicas Dentro', '1605201605519496447708.jpg', NULL, 'mobile-dicas-dentro', 'SIM'),
(15, 'Mobile Especificações', '1605201605222354732072.jpg', NULL, 'mobile-especificacoes', 'SIM'),
(16, 'Mobile Portfólio', '2603201612151276738505.jpg', NULL, 'mobile-portfolio', 'SIM'),
(17, 'Mobile Portfólio Dentro', '2603201612161191731379.jpg', NULL, 'mobile-portfolio-dentro', 'SIM'),
(18, 'Mobile Fale Conosco', '1605201605189744556703.jpg', NULL, 'mobile-fale-conosco', 'SIM'),
(19, 'Mobile Trabalhe Conosco', '1605201605199561320232.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM'),
(20, 'Orçamento', '1605201605512617472790.jpg', NULL, 'orcamento', 'SIM'),
(21, 'Mobile Orçamento', '1605201605198513822961.jpg', NULL, 'mobile-orcamento', 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoriaproduto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=75 ;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(70, 'Portas de Aço', NULL, '2005201602447838896318.png', 'SIM', NULL, 'portas-de-aco', 'Portas de Aço', 'Portas de Aço', 'Portas de Aço'),
(71, 'Portões de Aço de Enrolar', NULL, '0703201609411367031103.png', 'SIM', NULL, 'portoes-de-aco-de-enrolar', 'Portões de Aço de Enrolar', 'Portões de Aço de Enrolar Residencial, Portões de Aço de Enrolar Industrial, Portões Industriais, Portão Industrial, Portões de Garagem Americanas, Portões de Garagem de Enrolar, Portões de Aço de Enrolar Residencial DF, Portões de Aço de Enrolar Industri', 'Portões de Aço de Enrolar, Portões de Garagem de Enrolar.'),
(72, 'Estruturas Metálicas', NULL, '0703201609421240028963.png', 'SIM', NULL, 'estruturas-metalicas', NULL, NULL, NULL),
(74, 'Alambrados Grades', NULL, '2005201602425814033312.png', 'SIM', NULL, 'alambrados-grades', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idcomentariodica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idcomentarioproduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `nome_tb_sitemaps` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idconfiguracao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `titulo`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`, `nome_tb_sitemaps`, `google_plus`) VALUES
(1, NULL, 'SIM', 0, '', 'Rua 03 Chácara 86 Lote 06 Comercial Vicente Pires - Taguatinga - DF.\n', '(61) 3263-7207', '(61) 3877-7207', 'contato@liderportaseportoes.com.br', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15354.838548205209!2d-48.03207629877015!3d-15.819257272762599!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x082f00341d049844!2sL%C3%ADder+Portas+de+A%C3%A7o+DF!5e0!3m2!1spt-BR!2sbr!4v14621183', NULL, NULL, 'suporte@masmidia.com.br, angela.homeweb@gmail.com', '(62) 99228-6845', '', NULL, 'https://plus.google.com/+LíderPortasdeAçoePortõesEletrônicosBrasília');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iddepoimento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'João Paulo', '<p>\r\n	Jo&atilde;o Paulo Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'joao-paulo', '1703201603181368911340..jpg'),
(2, 'Ana Júlia', '<p>\r\n	Ana J&uacute;lia&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'ana-julia', '0803201607301356333056..jpg'),
(3, 'Tatiana Alves', '<p>\r\n	Tatiana&nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'tatiana-alves', '0803201607301343163616.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`iddica`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(42, 'Portão eletrônico é uma boa alternativa para segurança?', '<p style="text-align: justify;">\r\n	<strong>Quais as vantagens e desvantagens de ter o port&atilde;o eletr&ocirc;nico como uma alternativa para seguran&ccedil;a? Ser&aacute; que d&aacute; mesmo para confiar nessa estrat&eacute;gia em rela&ccedil;&atilde;o aos port&otilde;es manuais?</strong></p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	O pa&iacute;s que vivemos, o Brasil, n&atilde;o &eacute; um local sem leis onde n&atilde;o h&aacute; seguran&ccedil;a alguma, mas tamb&eacute;m n&atilde;o &eacute; um local onde as pessoas podem ir e vir livremente. Vivemos este paradoxo entre a seguran&ccedil;a que a pr&oacute;pria constitui&ccedil;&atilde;o nos garante e a inseguran&ccedil;a que &eacute; vis&iacute;vel especialmente nas cidades de m&eacute;dio e grande porte. Isto leva as pessoas a adotarem todo tipo de recurso para garantir sua pr&oacute;pria seguran&ccedil;a e da sua fam&iacute;lia, como blindar carros, erguer muros, cerca el&eacute;trica, port&otilde;es autom&aacute;ticos, c&acirc;meras de seguran&ccedil;a, entre outros.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>Port&otilde;es eletr&ocirc;nicos</strong></p>\r\n<p style="text-align: justify;">\r\n	O uso de port&otilde;es eletr&ocirc;nicos por exemplos &eacute; adotado por muitos como pura conveni&ecirc;ncia para os dias de chuva ou frio, mas tamb&eacute;m pode ser usado como estrat&eacute;gia de seguran&ccedil;a para evitar roubos quando a pessoa est&aacute; chegando ou saindo de casa. Mas quais as vantagens deste tipo de estrat&eacute;gia e ser&aacute; mesmo que o port&atilde;o autom&aacute;tico pode ser seguro para os moradores?</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Primeiro que a ideia usada &eacute; que nos casos de port&otilde;es manuais h&aacute; um tempo grande para descer do carro, abrir o port&atilde;o, entrar no carro, coloc&aacute;-lo para dentro, descer novamente e fechar o port&atilde;o. No caso do port&atilde;o eletr&ocirc;nico voc&ecirc; reduz e muito este tempo garantindo que ele fique aberto o menor tempo poss&iacute;vel.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>Vantagens e desvantagens</strong></p>\r\n<p style="text-align: justify;">\r\n	Como vantagem est&aacute; o fato apresentado acima que &eacute; voc&ecirc; fazer o processo de entrada ou sa&iacute;da da garagem no menor tempo poss&iacute;vel, sem contar os aspectos da conveni&ecirc;ncia tamb&eacute;m citados acima. Como desvantagens existe a falsa sensa&ccedil;&atilde;o de seguran&ccedil;a, j&aacute; que mesmo com o port&atilde;o autom&aacute;tico existe ainda um tempo que ele ficar&aacute; aberto e preciso entender que ele ajuda, mas n&atilde;o resolve todo o problema da inseguran&ccedil;a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>Pontos a observar</strong></p>\r\n<p style="text-align: justify;">\r\n	Qual o tempo de abertura e fechamento do port&atilde;o? Este &eacute; um dos pontos principais para ser analisado na compra.</p>\r\n<p style="text-align: justify;">\r\n	Qual a posi&ccedil;&atilde;o da sua casa dentro do quarteir&atilde;o? Quanto mais pr&oacute;ximo da esquina, mais dif&iacute;cil &eacute; a quest&atilde;o da seguran&ccedil;a em rela&ccedil;&atilde;o ao port&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	A frente da sua casa proporciona uma vis&atilde;o livre para os dos lados da rua? Quando a vis&atilde;o &eacute; obstru&iacute;da tamb&eacute;m tende a complicar mais.</p>\r\n<p style="text-align: justify;">\r\n	<strong>Conclus&atilde;o</strong></p>\r\n<p style="text-align: justify;">\r\n	Acredito que adotar o port&atilde;o eletr&ocirc;nico seja sim uma boa alternativa, mas outras estrat&eacute;gias tamb&eacute;m devem ser adotadas em conjunto como por exemplo a prud&ecirc;ncia de dar uma volta ao quarteir&atilde;o antes de chegar em casa, isto especialmente a noite.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fonte:&nbsp;http://www.casadicas.com.br/bem-estar/portao-eletronico-e-uma-boa-alternativa-para-seguranca/</p>', '1205201602131421797817..jpg', 'SIM', NULL, 'portao-eletronico-e-uma-boa-alternativa-para-seguranca', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', NULL),
(43, 'Como fazer a manutenção de uma porta de enrolar', '<p style="text-align: justify;">\r\n	<strong>Como fazer a manuten&ccedil;&atilde;o de uma porta de enrolar</strong></p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Um dos grandes segredos para que todo tipo de produto dure por muito mais tempo &eacute; justamente fazer a manuten&ccedil;&atilde;o de forma correta e com as portas de enrolar n&atilde;o &eacute; diferente e mesmo com um produto dur&aacute;vel como este, &eacute; de suma import&acirc;ncia que se fa&ccedil;a a manuten&ccedil;&atilde;o, para que o tempo &uacute;til seja bem maior e no artigo abaixo teremos algumas dicas interessantes de como fazer este tipo de manuten&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>Manuten&ccedil;&atilde;o Portas de Enrolar</strong></p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Um dos produtos mais importantes em um com&eacute;rcio de qualquer tipo de tamanho &eacute; justamente uma porta de enrolar, que al&eacute;m de trazer mais seguran&ccedil;a para o estabelecimento, agrega muito valor para o lugar e para que esta porta seja &uacute;til por muito tempo, abaixo v&atilde;o algumas dicas bem importantes para fazer a manuten&ccedil;&atilde;o correta na porta de enrolar.</p>\r\n<p style="text-align: justify;">\r\n	Procure sempre contar com profissionais que seja capacitado para fazer este tipo de manuten&ccedil;&atilde;o, pois geralmente as melhores empresas do mercado contam com um prazo de garantia e com profissionais que sabem mexer com as portas de enrolar.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Os grandes riscos de contratar profissionais que n&atilde;o pertencem a empresa que fabricou a porta de enrolar comprada s&atilde;o os seguintes: a perca da garantia e o fato de que este profissional, por melhor que seja, n&atilde;o conhece direito a porta de enrolar desta empresa, o que aumenta e muito os riscos de surgir um problema maior no futuro.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Um bom profissional ir&aacute; fazer a manuten&ccedil;&atilde;o nas portas de enrolar de forma correta e minuciosa e a manuten&ccedil;&atilde;o n&atilde;o fica somente nos cuidados com as portas, mas com todo o conjunto que envolve o produto, como motores, sensores, controles remotos e muito mais.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Quando n&atilde;o se consegue abrir a porta de enrolar autom&aacute;tica durante o uso do controle remoto, um dos problemas pode ser que as pilha ou baterias estejam fracas, portanto &eacute; de suma import&acirc;ncia que a troca seja feita e &eacute; um dos procedimentos que n&atilde;o h&aacute; a necessidade de um t&eacute;cnico no local.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A falta de graxa nos v&atilde;os das portas pode ser um dos motivos pelos quais as portas fa&ccedil;am um ru&iacute;do que traz muito inc&ocirc;modo.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fonte:&nbsp;http://guiapoliticamenteincorreto.com/2015/01/06/como-fazer-a-manutencao-de-uma-porta-de-enrolar/</p>', '1205201602245809337860.jpeg', 'SIM', NULL, 'como-fazer-a-manutencao-de-uma-porta-de-enrolar', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', NULL),
(44, 'Porque usar Portas de Aço em dias atuais?', '<p style="text-align: justify;">\r\n	Qualquer pessoa que possui um estabelecimento comercial, especialmente aqueles que est&atilde;o localizados na rua, ou seja, que n&atilde;o est&atilde;o dentro de um determinado estabelecimento comercial, precisam pensar em seguran&ccedil;a. E dentre todos dispositivos modernos que podem ajudar as pessoas a manterem seus estoques e evitarem de serem roubadas, as portas continuam sendo fundamentais.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Existem diversos modelos de portas que podem ser instaladas em um determinado ponto comercial, mas os modelos mais utilizados acabam sendo os feitos de a&ccedil;o. Um dos principais benef&iacute;cios que as pessoas acabam tendo ao instalar este tipo de porta de a&ccedil;o &eacute; justamente a for&ccedil;a e a resist&ecirc;ncia da mesma.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Qualquer loja que conta com uma porta de a&ccedil;o acaba sendo mais complicada de ser entrada, o que pode fazer com que os bandidos e ladr&otilde;es pensem duas vezes antes de tentar adentrar.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Existem diversos modelos de portas de a&ccedil;o dispon&iacute;veis no mercado. Algumas delas contam com sistemas automatizados, que podem ser acionados apenas com um controle remoto, o que acaba sendo um elemento a mais de seguran&ccedil;a, j&aacute; que as ondas utilizadas pelos controles remotos s&atilde;o complicadas de serem descobertas.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Mas alguns comerciantes ainda preferem os modelos de portas de a&ccedil;o manuais. Elas tamb&eacute;m acabam oferecendo algumas vantagens interessantes, sendo que uma delas &eacute; o fato dos donos conseguirem trancar as portas mais facilmente, adaptando uma s&eacute;rie de outros m&eacute;todos para tornar a porta ainda mais complicada de ser aberta.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Existem alguns modelos tamb&eacute;m destinadas as lojas que ficam dentro de estabelecimentos comerciais, como os shoppings. Alguns modelos contam com uma certa dose de transpar&ecirc;ncia, que permite com que os consumidores consigam enxergar o que est&aacute; dentro da loja mesmo quando ela est&aacute; fechada. Pode ser um bom instrumento de seguran&ccedil;a, mas, de uma maneira geral, estas portas de a&ccedil;o costumam ser mais fr&aacute;geis, exigindo sistemas de seguran&ccedil;a externos.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Resumindo: a utiliza&ccedil;&atilde;o de portas de a&ccedil;o acaba sendo fundamental no dia a dia de empresas. Afinal de contas, elas s&atilde;o as portas mais seguras que podem ser encontradas no mercado. Al&eacute;m disso, as portas de a&ccedil;o ainda podem ser utilizadas com outros sistemas extras de seguran&ccedil;a.</p>\r\n<div>\r\n	Fonte:&nbsp;http://www.editora-animalworld.com.br/2015/08/porque-usar-portas-de-aco-em-dias-atuais/</div>', '1205201603179473616853..jpg', 'SIM', NULL, 'porque-usar-portas-de-aco-em-dias-atuais', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', NULL),
(45, 'Dica 4', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0803201609201338420865..jpg', 'NAO', NULL, 'dica-4', NULL, NULL, NULL, NULL),
(46, 'Dica 5', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0803201609201156418593..jpg', 'NAO', NULL, 'dica-5', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Index - Conheça mais a Lider portas e Portões', '<p>\r\n	Somos uma empresa que atua h&aacute; v&aacute;rios anos no mercado, com a filosofia de sempre contribuir para evolu&ccedil;&atilde;o tecnol&oacute;gica. Especializamos a cada dia para proporcionar trabalhos qualificados, com &eacute;tica e lealdade aos nossos clientes e parceiros, aliados a produtos de alta qualidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Oferecendo o que h&aacute; de melhor em portas de enrolar, port&otilde;es residenciais e industriais, automa&ccedil;&atilde;o, fechaduras el&eacute;tricas e serralheria em geral, garantindo aos nossos clientes portas de a&ccedil;o com qualidade de alto padr&atilde;o. Podendo ser instaladas no seu com&eacute;rcio com manuten&ccedil;&atilde;o quase zero. Nossas portas de a&ccedil;o s&atilde;o as melhores portas existentes no Brasil.</p>', 'SIM', 0, '', '', '', 'index--conheca-mais-a-lider-portas-e-portoes', NULL, NULL, NULL),
(2, 'Empresa - Legenda', '<p>\r\n	Empresa l&iacute;der com excel&ecirc;ncia de atendimento e moderniza&ccedil;&atilde;o</p>', 'SIM', 0, '', '', '', 'empresa--legenda', NULL, NULL, NULL),
(3, 'Empresa - Descrição', '<p>\r\n	Somos uma empresa que atua h&aacute; v&aacute;rios anos no mercado, com a filosofia de sempre contribuir para evolu&ccedil;&atilde;o tecnol&oacute;gica. Especializamos a cada dia para proporcionar trabalhos qualificados, com &eacute;tica e lealdade aos nossos clientes e parceiros, aliados a produtos de alta qualidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Oferecendo o que h&aacute; de melhor em portas de enrolar, port&otilde;es residenciais e industriais, automa&ccedil;&atilde;o, fechaduras el&eacute;tricas e serralheria em geral, garantindo aos nossos clientes portas de a&ccedil;o com qualidade de alto padr&atilde;o. Podendo ser instaladas no seu com&eacute;rcio com manuten&ccedil;&atilde;o quase zero. Nossas portas de a&ccedil;o s&atilde;o as melhores portas existentes no Brasil.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Somos uma empresa experiente que preza pela excel&ecirc;ncia na qualidade e atendimento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&bull; Invista em seu patrim&ocirc;nio com quem possui experi&ecirc;ncia e credibilidade no mercado.</p>', 'SIM', 0, '', '', '', 'empresa--descricao', NULL, NULL, NULL),
(4, 'Especificações - Legenda', '<p>\r\n	Conhe&ccedil;a um pouco mais sobre nossas portas atrav&eacute;s de desenhos t&eacute;cnicos.</p>', 'SIM', 0, '', '', '', 'especificacoes--legenda', NULL, NULL, NULL),
(5, 'Especificações - Legenda interna', '<p>\r\n	Especifica&ccedil;&otilde;es e desenhos t&eacute;cnicos</p>', 'SIM', 0, '', '', '', 'especificacoes--legenda-interna', NULL, NULL, NULL),
(6, 'Orçamento - Legenda', '<p>\r\n	Entre em contato e solicite or&ccedil;amento sem compromisso.</p>', 'SIM', 0, '', '', '', 'orcamento--legenda', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_especificacoes`
--

CREATE TABLE IF NOT EXISTS `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext,
  PRIMARY KEY (`idespecificacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', ''),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', ''),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', ''),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', ''),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgaleriaportifolio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '1503201609571300280060.jpg', 'SIM', NULL, NULL, 1),
(2, '1503201609571194123464.jpg', 'SIM', NULL, NULL, 1),
(3, '1503201609571223466219.jpg', 'SIM', NULL, NULL, 1),
(4, '1503201609571319150261.jpg', 'SIM', NULL, NULL, 1),
(5, '1503201609571312788443.jpg', 'SIM', NULL, NULL, 1),
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeriaproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=225 ;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(28, '1503201609561319044858.jpg', 'SIM', NULL, NULL, 3),
(29, '1503201609561123662081.jpg', 'SIM', NULL, NULL, 3),
(30, '1503201609561206848839.jpg', 'SIM', NULL, NULL, 3),
(31, '1503201609561331659456.jpg', 'SIM', NULL, NULL, 3),
(32, '1503201609561244715085.jpg', 'SIM', NULL, NULL, 3),
(33, '1503201609561286452532.jpg', 'SIM', NULL, NULL, 4),
(34, '1503201609561167966515.jpg', 'SIM', NULL, NULL, 4),
(35, '1503201609561317434222.jpg', 'SIM', NULL, NULL, 4),
(36, '1503201609561269009510.jpg', 'SIM', NULL, NULL, 4),
(37, '1503201609561274119030.jpg', 'SIM', NULL, NULL, 4),
(38, '1503201609561349582165.jpg', 'SIM', NULL, NULL, 5),
(39, '1503201609561326914421.jpg', 'SIM', NULL, NULL, 5),
(40, '1503201609561319011268.jpg', 'SIM', NULL, NULL, 5),
(41, '1503201609561360905328.jpg', 'SIM', NULL, NULL, 5),
(42, '1503201609561199681654.jpg', 'SIM', NULL, NULL, 5),
(46, '1205201601552054943471.jpg', 'SIM', NULL, NULL, 2),
(47, '1205201601556884588686.jpg', 'SIM', NULL, NULL, 2),
(48, '1205201602005679149470.jpg', 'SIM', NULL, NULL, 7),
(49, '1205201602005934120015.jpg', 'SIM', NULL, NULL, 7),
(55, '1205201605197105360908.jpg', 'SIM', NULL, NULL, 7),
(56, '1205201605364713407183.jpg', 'SIM', NULL, NULL, 7),
(58, '1205201605361702106694.jpg', 'SIM', NULL, NULL, 7),
(59, '1205201605401719320523.jpg', 'SIM', NULL, NULL, 7),
(60, '1605201605356149568263.jpg', 'SIM', NULL, NULL, 7),
(61, '1605201605395680508808.jpg', 'SIM', NULL, NULL, 8),
(62, '1605201605391593189980.jpg', 'SIM', NULL, NULL, 8),
(63, '1605201605394714048500.jpg', 'SIM', NULL, NULL, 8),
(64, '1605201605395033997884.jpg', 'SIM', NULL, NULL, 8),
(65, '1605201605394497372011.jpg', 'SIM', NULL, NULL, 8),
(67, '1605201605429852625984.jpg', 'SIM', NULL, NULL, 1),
(68, '1605201605421735210799.jpg', 'SIM', NULL, NULL, 1),
(69, '1605201605423388823626.jpg', 'SIM', NULL, NULL, 1),
(70, '1605201605422948196157.jpg', 'SIM', NULL, NULL, 1),
(71, '1605201605424205843611.jpg', 'SIM', NULL, NULL, 1),
(72, '1605201605426390703063.jpg', 'SIM', NULL, NULL, 1),
(73, '1805201601571822918549.jpg', 'SIM', NULL, NULL, 9),
(75, '1805201601571675037662.jpg', 'SIM', NULL, NULL, 9),
(76, '1805201601591146156862.jpg', 'SIM', NULL, NULL, 9),
(77, '1805201602037840027800.jpg', 'SIM', NULL, NULL, 10),
(78, '1905201602464617323887.jpg', 'SIM', NULL, NULL, 12),
(79, '1905201602466549118152.jpg', 'SIM', NULL, NULL, 11),
(80, '1905201602481173193736.jpg', 'SIM', NULL, NULL, 11),
(81, '1905201602492934984473.jpg', 'SIM', NULL, NULL, 12),
(82, '1905201602498920849801.jpg', 'SIM', NULL, NULL, 12),
(83, '0208201605208810253673.jpg', 'SIM', NULL, NULL, 9),
(84, '0208201605208819013084.jpg', 'SIM', NULL, NULL, 9),
(85, '0208201605201475988883.jpg', 'SIM', NULL, NULL, 9),
(87, '0208201605217762590195.jpg', 'SIM', NULL, NULL, 10),
(88, '0208201605219929849414.jpg', 'SIM', NULL, NULL, 10),
(89, '0208201605214608729858.jpg', 'SIM', NULL, NULL, 10),
(92, '0208201605241698909480.jpg', 'SIM', NULL, NULL, 11),
(93, '0208201605244836679659.jpg', 'SIM', NULL, NULL, 11),
(94, '0208201605242591301666.jpg', 'SIM', NULL, NULL, 11),
(95, '0208201605244713898996.jpg', 'SIM', NULL, NULL, 11),
(96, '0208201605256419519041.jpg', 'SIM', NULL, NULL, 11),
(97, '0208201605279911173101.jpg', 'SIM', NULL, NULL, 12),
(98, '0208201605271863346377.jpg', 'SIM', NULL, NULL, 12),
(99, '0208201605276273801041.jpg', 'SIM', NULL, NULL, 12),
(100, '0208201605272295068676.jpg', 'SIM', NULL, NULL, 12),
(104, '0208201605339081104704.jpg', 'SIM', NULL, NULL, 8),
(105, '0208201605334154017935.jpg', 'SIM', NULL, NULL, 8),
(106, '0208201605337871146798.jpg', 'SIM', NULL, NULL, 8),
(107, '0208201605336355570476.jpg', 'SIM', NULL, NULL, 8),
(109, '0208201605341403506621.jpg', 'SIM', NULL, NULL, 8),
(110, '0208201605348242819077.jpg', 'SIM', NULL, NULL, 8),
(111, '0208201605346920826745.jpg', 'SIM', NULL, NULL, 8),
(112, '0208201605341535620552.jpg', 'SIM', NULL, NULL, 8),
(116, '0208201605373102025704.jpg', 'SIM', NULL, NULL, 10),
(117, '0208201605378513785447.jpg', 'SIM', NULL, NULL, 10),
(118, '0208201605379307955726.jpg', 'SIM', NULL, NULL, 10),
(119, '0208201605373478301072.jpg', 'SIM', NULL, NULL, 10),
(120, '0208201605381222440207.jpg', 'SIM', NULL, NULL, 10),
(121, '0208201605388175113834.jpg', 'SIM', NULL, NULL, 10),
(123, '0208201605386662347478.jpg', 'SIM', NULL, NULL, 10),
(124, '0208201605406003426367.jpg', 'SIM', NULL, NULL, 10),
(125, '0208201605404536721313.jpg', 'SIM', NULL, NULL, 10),
(126, '0208201605443577306758.jpg', 'SIM', NULL, NULL, 9),
(127, '0208201605483135513886.jpg', 'SIM', NULL, NULL, 10),
(135, '0609201612494400607736.jpg', 'SIM', NULL, NULL, 14),
(136, '0609201612492841455278.jpg', 'SIM', NULL, NULL, 14),
(144, '0609201612575544828929.jpg', 'SIM', NULL, NULL, 13),
(145, '0609201612572889259159.jpg', 'SIM', NULL, NULL, 13),
(146, '0609201612574546786492.jpg', 'SIM', NULL, NULL, 13),
(147, '0609201612579334223825.jpg', 'SIM', NULL, NULL, 13),
(148, '0609201612579607798573.jpg', 'SIM', NULL, NULL, 13),
(149, '0609201612577867138137.jpg', 'SIM', NULL, NULL, 13),
(150, '0609201612578258311313.jpg', 'SIM', NULL, NULL, 13),
(151, '0609201612585904786139.jpg', 'SIM', NULL, NULL, 15),
(152, '0609201612585623980284.jpg', 'SIM', NULL, NULL, 15),
(153, '0609201612584535309784.jpg', 'SIM', NULL, NULL, 15),
(154, '0609201612586380099393.jpg', 'SIM', NULL, NULL, 15),
(155, '0609201612581246608892.jpg', 'SIM', NULL, NULL, 15),
(156, '0609201612584072366000.jpg', 'SIM', NULL, NULL, 15),
(157, '0609201612582454725623.jpg', 'SIM', NULL, NULL, 15),
(159, '0609201601029486888104.jpg', 'SIM', NULL, NULL, 8),
(160, '0609201601025471788019.jpg', 'SIM', NULL, NULL, 8),
(161, '0609201601026163561075.jpg', 'SIM', NULL, NULL, 8),
(162, '0609201601029104499965.jpg', 'SIM', NULL, NULL, 8),
(163, '0609201601027092597463.jpg', 'SIM', NULL, NULL, 8),
(164, '0609201601078089894648.jpg', 'SIM', NULL, NULL, 9),
(165, '0609201601078712172019.jpg', 'SIM', NULL, NULL, 9),
(166, '0609201601085009390376.jpg', 'SIM', NULL, NULL, 9),
(167, '0609201601082905977190.jpg', 'SIM', NULL, NULL, 9),
(168, '0609201601081991091519.jpg', 'SIM', NULL, NULL, 9),
(169, '0609201601081334351619.jpg', 'SIM', NULL, NULL, 9),
(170, '0609201601091405425589.jpg', 'SIM', NULL, NULL, 9),
(171, '0609201601111394232399.jpg', 'SIM', NULL, NULL, 8),
(172, '0609201601114253453065.jpg', 'SIM', NULL, NULL, 10),
(173, '0609201601148807387517.jpg', 'SIM', NULL, NULL, 11),
(175, '0609201601144893429196.jpg', 'SIM', NULL, NULL, 11),
(176, '0609201601143183640746.jpg', 'SIM', NULL, NULL, 11),
(177, '0609201601192197314347.jpg', 'SIM', NULL, NULL, 9),
(178, '0609201601205680398556.jpg', 'SIM', NULL, NULL, 8),
(179, '0609201601206604954455.jpg', 'SIM', NULL, NULL, 12),
(180, '0609201601203229764232.jpg', 'SIM', NULL, NULL, 12),
(181, '0609201601202013261661.jpg', 'SIM', NULL, NULL, 12),
(182, '0609201601203202706848.jpg', 'SIM', NULL, NULL, 12),
(183, '0609201601206292848653.jpg', 'SIM', NULL, NULL, 12),
(184, '0609201601209973619745.jpg', 'SIM', NULL, NULL, 12),
(185, '0609201601215998863987.png', 'SIM', NULL, NULL, 12),
(186, '0609201601222908846284.jpg', 'SIM', NULL, NULL, 12),
(187, '0609201601241786838045.jpg', 'SIM', NULL, NULL, 13),
(188, '0609201601245614346100.jpg', 'SIM', NULL, NULL, 13),
(189, '0609201601253856936912.jpg', 'SIM', NULL, NULL, 11),
(190, '0609201601254836970960.jpg', 'SIM', NULL, NULL, 13),
(191, '0609201601324302711052.jpg', 'SIM', NULL, NULL, 13),
(192, '0609201601325941165188.jpg', 'SIM', NULL, NULL, 13),
(193, '0609201601325118405715.jpg', 'SIM', NULL, NULL, 13),
(194, '0609201601328407816373.jpg', 'SIM', NULL, NULL, 13),
(195, '0609201601322154147426.jpeg', 'SIM', NULL, NULL, 13),
(196, '0609201601324223409249.jpg', 'SIM', NULL, NULL, 13),
(197, '0609201601322760360261.jpg', 'SIM', NULL, NULL, 13),
(198, '0609201601336816409495.jpg', 'SIM', NULL, NULL, 13),
(199, '0609201601332907540586.jpg', 'SIM', NULL, NULL, 13),
(200, '0609201601336431423785.jpg', 'SIM', NULL, NULL, 13),
(201, '0609201601335230866153.jpg', 'SIM', NULL, NULL, 13),
(202, '0609201601337457962760.jpg', 'SIM', NULL, NULL, 13),
(203, '0609201601337385921610.jpg', 'SIM', NULL, NULL, 13),
(204, '0609201601337162757768.jpg', 'SIM', NULL, NULL, 13),
(205, '0609201601337512600363.jpg', 'SIM', NULL, NULL, 13),
(206, '0609201601331815758661.jpg', 'SIM', NULL, NULL, 13),
(207, '0609201601336730985802.jpg', 'SIM', NULL, NULL, 13),
(208, '0609201601335026244305.jpg', 'SIM', NULL, NULL, 13),
(209, '0609201601337941645408.jpg', 'SIM', NULL, NULL, 13),
(210, '0609201601335849750675.jpg', 'SIM', NULL, NULL, 13),
(211, '0609201601331328986142.jpg', 'SIM', NULL, NULL, 13),
(212, '0609201601335241991558.jpg', 'SIM', NULL, NULL, 13),
(213, '0609201601339213873181.jpg', 'SIM', NULL, NULL, 13),
(214, '0609201601339966122342.jpg', 'SIM', NULL, NULL, 13),
(215, '0609201601339858226660.jpg', 'SIM', NULL, NULL, 13),
(216, '0609201601332015669552.jpg', 'SIM', NULL, NULL, 13),
(217, '0609201601333852098203.jpg', 'SIM', NULL, NULL, 13),
(218, '0609201601352068306145.jpg', 'SIM', NULL, NULL, 13),
(219, '0609201601366193309057.jpg', 'SIM', NULL, NULL, 13),
(220, '0609201601368678500726.jpg', 'SIM', NULL, NULL, 13),
(221, '1502201701262961091229.jpg', 'SIM', NULL, NULL, 16),
(222, '1502201701266767830571.jpg', 'SIM', NULL, NULL, 16),
(223, '1502201701266787114130.jpg', 'SIM', NULL, NULL, 16),
(224, '1502201701265595033048.jpg', 'SIM', NULL, NULL, 16);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`) VALUES
(1, 'Homeweb', 'c763face40f3a9bb4e53214c6e95edf5', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', 'homeweb'),
(2, 'Marcio', '0d5f9f3dea77e75e4288c9042f2b0e52', 'SIM', 0, 'suporte@masmidia.com.br', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`idloglogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=382 ;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''3''', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''3''', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''4''', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''4''', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''4''', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''4''', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''5''', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''6''', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''6''', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''6''', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''43''', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''43''', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''44''', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''44''', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''46''', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''45''', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''46''', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''45''', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''3''', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = ''NAO'' WHERE idlogin = ''2''', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = ''SIM'' WHERE idlogin = ''2''', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''4''', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''5''', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''6''', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'', id_grupologin = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'', id_grupologin = '''' WHERE idlogin = ''2''', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = ''2'', email = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''4''', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''5''', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '04:49:12', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '05:00:34', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '05:01:26', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '05:01:53', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '16:03:37', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:46:43', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:47:17', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:47:26', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:47:34', 1),
(159, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:47:52', 1),
(160, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:48:01', 1),
(161, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:48:10', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:48:21', 1),
(163, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:48:29', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '00:49:23', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '01:42:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '01:43:21', 1),
(167, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '01:44:07', 1),
(168, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '01:57:06', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '02:02:05', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '02:02:25', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-01', '12:58:21', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-01', '13:00:36', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-01', '13:42:15', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '01:41:21', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '01:54:18', 1),
(176, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '01:55:30', 1),
(177, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '01:55:58', 1),
(178, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '01:59:43', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '02:02:50', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '02:13:23', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '02:14:00', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '02:24:13', 1),
(183, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''45''', '2016-05-12', '02:24:43', 1),
(184, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''46''', '2016-05-12', '02:24:46', 1),
(185, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''44''', '2016-05-12', '02:24:50', 1),
(186, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '02:45:12', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '02:57:31', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '03:02:55', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '03:15:39', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '03:17:47', 1),
(191, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''44''', '2016-05-12', '03:17:53', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '04:53:00', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '04:56:52', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '05:03:20', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:15:08', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:15:16', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:15:24', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:15:32', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:15:50', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:15:59', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:16:07', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:16:20', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:16:27', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:16:36', 1),
(205, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:16:43', 1),
(206, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:16:50', 1),
(207, 'CADASTRO DO CLIENTE ', '', '2016-05-12', '13:23:16', 1),
(208, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:23:49', 1),
(209, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:25:11', 1),
(210, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:25:27', 1),
(211, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '14:02:31', 1),
(212, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '16:56:03', 1),
(213, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '16:56:43', 1),
(214, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '16:57:38', 1),
(215, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '17:09:23', 1),
(216, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '17:10:32', 1),
(217, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '18:40:40', 1),
(218, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '18:41:43', 1),
(219, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '22:02:27', 1),
(220, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '03:08:03', 1),
(221, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '03:29:47', 1),
(222, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '03:30:39', 1),
(223, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '03:32:34', 1),
(224, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '03:46:16', 1),
(225, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '03:51:43', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '03:53:40', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '03:59:38', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:10:26', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:18:12', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:25:37', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:36:32', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:37:36', 1),
(233, 'CADASTRO DO CLIENTE ', '', '2016-05-16', '04:40:25', 1),
(234, 'CADASTRO DO CLIENTE ', '', '2016-05-16', '04:45:51', 1),
(235, 'DESATIVOU O LOGIN 45', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''45''', '2016-05-16', '04:47:11', 1),
(236, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:52:36', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:54:14', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:57:21', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '04:59:12', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:00:51', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:03:41', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:05:36', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:07:06', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:08:27', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:18:30', 1),
(246, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:19:02', 1),
(247, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:19:17', 1),
(248, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:20:47', 1),
(249, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:21:00', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:22:34', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:24:57', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:26:30', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:28:03', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:29:53', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:31:23', 1),
(256, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:31:47', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:32:33', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:46:24', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:48:54', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:50:01', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:51:06', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:51:39', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '05:52:16', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:07:54', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:08:04', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:08:14', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:08:24', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:08:32', 1),
(269, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:08:40', 1),
(270, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:08:48', 1),
(271, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:04:07', 1),
(272, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:05:26', 1),
(273, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:05:55', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:06:15', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:06:39', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:07:14', 1),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:07:34', 1),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:08:07', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:12:50', 1),
(280, 'CADASTRO DO CLIENTE ', '', '2016-05-18', '13:55:33', 1),
(281, 'CADASTRO DO CLIENTE ', '', '2016-05-18', '14:03:14', 1),
(282, 'CADASTRO DO CLIENTE ', '', '2016-05-19', '14:40:40', 0),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:42:02', 0),
(284, 'CADASTRO DO CLIENTE ', '', '2016-05-19', '14:44:57', 0),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:56:37', 0),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:57:39', 0),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '15:12:39', 0),
(288, 'CADASTRO DO CLIENTE ', '', '2016-05-20', '02:42:39', 0),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:43:48', 0),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:43:59', 0),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:44:50', 0),
(292, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''73''', '2016-05-20', '02:45:01', 0),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:45:15', 0),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:45:59', 0),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:46:11', 0),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:46:23', 0),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:46:39', 0),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:47:06', 0),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:47:33', 0),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:47:47', 0),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '02:48:06', 0),
(302, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''1''', '2016-09-06', '12:46:07', 1),
(303, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '12:46:42', 1),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '12:47:18', 1),
(305, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''2''', '2016-09-06', '12:49:01', 1),
(306, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '12:49:33', 1),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '12:50:17', 1),
(308, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''7''', '2016-09-06', '12:52:24', 1),
(309, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '12:53:18', 1),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '12:56:32', 1),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '12:58:05', 1),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '13:38:11', 1),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '13:38:23', 1),
(314, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '18:30:18', 0),
(315, 'DESATIVOU O LOGIN 51', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''51''', '2016-09-06', '18:31:20', 0),
(316, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '18:35:31', 0),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '18:38:59', 0),
(318, 'DESATIVOU O LOGIN 48', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''48''', '2016-09-06', '18:39:32', 0),
(319, 'DESATIVOU O LOGIN 49', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''49''', '2016-09-06', '18:39:36', 0),
(320, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '18:42:28', 0),
(321, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '18:45:18', 0),
(322, 'DESATIVOU O LOGIN 53', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''53''', '2016-09-06', '18:46:04', 0),
(323, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '18:51:22', 0),
(324, 'ATIVOU O LOGIN 53', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''53''', '2016-09-06', '18:51:28', 0),
(325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '18:52:35', 0),
(326, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '19:18:34', 0),
(327, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '19:19:30', 0),
(328, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '19:23:05', 0),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '19:28:18', 0),
(330, 'DESATIVOU O LOGIN 42', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''42''', '2016-09-06', '19:29:04', 0),
(331, 'ATIVOU O LOGIN 42', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''42''', '2016-09-06', '19:29:12', 0),
(332, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '19:29:36', 0),
(333, 'DESATIVOU O LOGIN 54', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''54''', '2016-09-06', '19:29:44', 0),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '19:30:43', 0),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '19:31:03', 0),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '19:32:08', 0),
(337, 'DESATIVOU O LOGIN 53', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''53''', '2016-09-06', '19:32:39', 0),
(338, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '19:48:08', 0),
(339, 'DESATIVOU O LOGIN 55', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''55''', '2016-09-06', '19:48:39', 0),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '19:50:30', 0),
(341, 'ATIVOU O LOGIN 55', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''55''', '2016-09-06', '19:50:36', 0),
(342, 'DESATIVOU O LOGIN 55', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''55''', '2016-09-06', '19:51:13', 0),
(343, 'CADASTRO DO CLIENTE ', '', '2016-09-06', '19:57:28', 0),
(344, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:03:40', 0),
(345, 'ATIVOU O LOGIN 55', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''55''', '2016-09-06', '20:03:46', 0),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:16:11', 0),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:17:28', 0),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:18:15', 0),
(349, 'EXCLUSÃO DO LOGIN 51, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''51''', '2016-09-06', '20:32:20', 0),
(350, 'EXCLUSÃO DO LOGIN 53, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''53''', '2016-09-06', '20:32:32', 0),
(351, 'EXCLUSÃO DO LOGIN 54, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''54''', '2016-09-06', '20:32:36', 0),
(352, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:37:16', 0),
(353, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:38:25', 0),
(354, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:38:57', 0),
(355, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:40:34', 0),
(356, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:42:24', 0),
(357, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:45:34', 0),
(358, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:46:22', 0),
(359, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:49:59', 0),
(360, 'DESATIVOU O LOGIN 50', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''50''', '2016-09-06', '20:50:08', 0),
(361, 'ATIVOU O LOGIN 45', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''45''', '2016-09-06', '20:50:28', 0),
(362, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:55:37', 0),
(363, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '20:59:17', 1),
(364, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '21:00:38', 1),
(365, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '21:02:59', 1),
(366, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '21:04:06', 1),
(367, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '21:06:25', 1),
(368, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '21:06:53', 1),
(369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '21:07:08', 1),
(370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '21:09:47', 1),
(371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-06', '21:10:56', 1),
(372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '21:20:53', 1),
(373, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-14', '12:04:42', 1),
(374, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-14', '12:05:11', 1),
(375, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-15', '13:22:20', 1),
(376, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-15', '13:22:50', 1),
(377, 'CADASTRO DO CLIENTE ', '', '2017-02-15', '13:25:03', 1),
(378, 'ALTERAÇÃO DO CLIENTE ', '', '2018-02-02', '11:56:41', 2),
(379, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: marciomas@gmail.com', 'DELETE FROM tb_logins WHERE idlogin = ''2''', '2018-03-12', '17:25:04', 1),
(380, 'ALTERAÇÃO DO LOGIN 1', '', '2018-03-12', '17:25:16', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2018-03-22', '09:35:37', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portfolios`
--

CREATE TABLE IF NOT EXISTS `tb_portfolios` (
  `idportfolio` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idportfolio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `tb_portfolios`
--

INSERT INTO `tb_portfolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Portão automático basculante articulado', '1503201609451124075050..jpg', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'portao-automatico-basculante-articulado'),
(2, 'Portão basculante', '1503201609451146973223..jpg', '<p>\r\n	Port&atilde;o basculante&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portão basculante', 'Portão basculante', 'Portão basculante', 'SIM', NULL, 'portao-basculante'),
(3, 'Fabricas portas aço', '1503201609461282883881..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', NULL, 'fabricas-portas-aco'),
(4, 'Portões de madeira', '1503201609501367441206..jpg', '<p>\r\n	Port&otilde;es de madeira&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões de madeira', 'Portões de madeira', 'Portões de madeira', 'SIM', NULL, 'portoes-de-madeira'),
(5, 'Portões automáticos', '1503201609511206108237..jpg', '<p>\r\n	Port&otilde;es autom&aacute;ticos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões automáticos', 'Portões automáticos', 'Portões automáticos', 'SIM', NULL, 'portoes-automaticos'),
(6, 'Portfólio 1', '1603201602001115206967..jpg', '<p>\r\n	Portf&oacute;lio 1</p>', 'Portfólio 1', 'Portfólio 1', 'Portfólio 1', 'SIM', NULL, 'portfolio-1'),
(7, 'Portfólio 2', '1603201602001357146508..jpg', '<p>\r\n	Portf&oacute;lio 2</p>', 'Portfólio 2', 'Portfólio 2', 'Portfólio 2', 'SIM', NULL, 'portfolio-2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`) VALUES
(8, 'Portas de Aço Automatizadas', '1205201602023791498607..jpg', '<p style="text-align: justify;">\r\n	As portas de a&ccedil;o de enrolar s&atilde;o cada vez mais utilizadas em ind&uacute;strias, com&eacute;rcios e resid&ecirc;ncias pelo fato de proporcionarem maior efici&ecirc;ncia e praticidade, utilizando-se da mais alta tecnologia dispon&iacute;vel no mercado.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	As portas de a&ccedil;o de enrolar s&atilde;o fabricadas seguindo os m&eacute;todos mais r&iacute;gidos, permitindo maior robustez, durabilidade e resist&ecirc;ncia. Podendo ser fabricadas de acordo com a solicita&ccedil;&atilde;o de cada cliente, sendo manual ou autom&aacute;tica (motores de alta tecnologia).&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p style="text-align: justify;">\r\n	Formas de Pagamento:</p>\r\n<p style="text-align: justify;">\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>\r\n<div>\r\n	&nbsp;</div>', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', 0, 'portas-de-aco-automatizadas', 70, NULL, NULL, NULL),
(9, 'Portas de aço Transvision', '1805201601556110586772..jpg', '<p style="text-align: justify;">\r\n	As portas de a&ccedil;o Transvision permitem a entrada de boa parte da luz e do ar dos ambientes externos para o interior da loja. O aspecto da porta semistransparente permite ver tudo o que acontece do lado de fora. Da mesma forma garantindo o lado est&eacute;tico pois se o interior da loja estiver iluminado e o lado de fora escuro, &eacute; poss&iacute;vel ver os produtos expostos de forma segura.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	As portas de a&ccedil;o de enrolar Transvision s&atilde;o cada vez mais utilizadas em com&eacute;rcios pelo fato de proporcionarem maior efici&ecirc;ncia e praticidade, utilizando-se da mais alta tecnologia dispon&iacute;vel no mercado.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	As portas de a&ccedil;o de enrolar Transvision s&atilde;o fabricadas seguindo os m&eacute;todos mais r&iacute;gidos, permitindo maior robustez, durabilidade e resist&ecirc;ncia. Podendo ser fabricadas de acordo com a solicita&ccedil;&atilde;o de cada cliente, sendo manual ou autom&aacute;tica (motores de alta tecnologia).&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p style="text-align: justify;">\r\n	Formas de Pagamento:</p>\r\n<p style="text-align: justify;">\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>', '', '', '', 'SIM', 0, 'portas-de-aco-transvision', 70, NULL, NULL, NULL),
(10, 'Portas de Aço Tijolinho', '1805201602034922056897..jpg', '<p style="text-align: justify;">\r\n	&nbsp;As portas de a&ccedil;o de enrolar tipo tijolinho s&atilde;o cada vez mais utilizadas em ind&uacute;strias e com&eacute;rcios pelo fato de proporcionarem maior efici&ecirc;ncia e praticidade, utilizando-se da mais alta tecnologia dispon&iacute;vel no mercado.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	As portas de a&ccedil;o de enrolar s&atilde;o fabricadas seguindo os m&eacute;todos mais r&iacute;gidos, permitindo maior robustez, durabilidade e resist&ecirc;ncia. Podendo ser fabricadas de acordo com a solicita&ccedil;&atilde;o de cada cliente, sendo manual ou autom&aacute;tica (motores de alta tecnologia).&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p style="text-align: justify;">\r\n	Formas de Pagamento:</p>\r\n<p style="text-align: justify;">\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>', '', '', '', 'SIM', 0, 'portas-de-aco-tijolinho', 70, NULL, NULL, NULL),
(11, 'Portões de Enrolar Industriais', '1905201602406313150839..jpg', '<p style="text-align: justify;">\r\n	Os port&otilde;es de enrolar de a&ccedil;o s&atilde;o cada vez mais utilizadas em ind&uacute;strias e com&eacute;rcios pelo fato de proporcionarem maior efici&ecirc;ncia e praticidade, utilizando-se da mais alta tecnologia dispon&iacute;vel no mercado.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Os port&otilde;es de enrolar de a&ccedil;o s&atilde;o fabricadas seguindo os m&eacute;todos mais r&iacute;gidos, permitindo maior robustez, durabilidade e resist&ecirc;ncia. Podendo ser fabricadas de acordo com a solicita&ccedil;&atilde;o de cada cliente, sendo manual ou autom&aacute;tica (motores de alta tecnologia).&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p style="text-align: justify;">\r\n	Formas de Pagamento:</p>\r\n<p style="text-align: justify;">\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>', '', '', '', 'SIM', 0, 'portoes-de-enrolar-industriais', 71, NULL, NULL, NULL),
(12, 'Portões de Enrolar Residenciais', '1905201602448984831840..jpg', '<p style="text-align: justify;">\r\n	Os port&otilde;es de enrolar de a&ccedil;o proporcionam a voc&ecirc; e sua fam&iacute;lia mais acessibilidade na hora de entrar e sair com seu carro em sua resid&ecirc;ncia, com maior efici&ecirc;ncia e praticidade, utilizando-se da mais alta tecnologia dispon&iacute;vel no mercado.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Os port&otilde;es de enrolar de a&ccedil;o s&atilde;o fabricadas seguindo os m&eacute;todos mais r&iacute;gidos, permitindo maior robustez, durabilidade e resist&ecirc;ncia. Podendo ser fabricadas de acordo com a solicita&ccedil;&atilde;o de cada cliente, sendo manual ou autom&aacute;tica (motores de alta tecnologia).&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p style="text-align: justify;">\r\n	Formas de Pagamento:</p>\r\n<p style="text-align: justify;">\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>', '', '', '', 'SIM', 0, 'portoes-de-enrolar-residenciais', 71, NULL, NULL, NULL),
(13, 'Portões de Garagem de Aço', '0609201612564147976529..jpg', '<p>\r\n	O port&atilde;o eletr&ocirc;nico &eacute; ideal para quem deseja ter seguran&ccedil;a em sua casa, favorecendo a voc&ecirc; e sua fam&iacute;lia mais acessibilidade na hora de entrar e sair com seu carro.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&Eacute; muito chato descer do carro toda vez que chega em casa para abrir o port&atilde;o, n&atilde;o &eacute; mesmo? Principalmente, se estiver chovendo, e com o port&atilde;o eletr&ocirc;nico voc&ecirc; tem muitas vantagens por t&ecirc;-lo em sua resid&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Vale a pena investir um pouco e obter mais seguran&ccedil;a. Voc&ecirc; ter&aacute; mais facilidade para entrar e sair, de carro ou at&eacute; mesmo a p&eacute;, com apenas um simples toque. O port&atilde;o eletr&ocirc;nico &eacute; projetado com um desing inovador para dar mais modernidade a sua casa, al&eacute;m da m&aacute;xima seguran&ccedil;a que voc&ecirc; e sua fam&iacute;lia precisa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Voc&ecirc; pode programar para abr&iacute;-lo de diferentes formas. Dependendo do port&atilde;o, abrir para cima, para o lado, de uma maneira muito mais segura e pr&aacute;tica. Veja alguns modelos de port&otilde;es eletr&ocirc;nicos para voc&ecirc;, que deseja manter sua casa muito mais segura e moderna.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma dica sempre importante &eacute; manter sempre o controle remoto longe do alcance de crian&ccedil;as e animais, evitando assim danos ao equipamento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p>\r\n	Formas de Pagamento:</p>\r\n<p>\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>\r\n<div>\r\n	&nbsp;</div>', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', 0, 'portoes-de-garagem-de-aco', 71, NULL, NULL, NULL),
(14, 'Estruturas Metálicas', '0609201612497810432683..jpg', '<p>\r\n	Um galp&atilde;o met&aacute;lico pode ser utilizado para diversas finalidades. A sua estrutura &eacute; basicamente em perfis met&aacute;licos, dimensionados conforme as necessidades de v&atilde;os, condi&ccedil;&otilde;es de utiliza&ccedil;&atilde;o, intemp&eacute;ries (chuvas, ventos, terremotos, temperaturas, etc.). Na cobertura pode-se utilizar telhas, sendo as mais usuais, o a&ccedil;o (galvanizado), alum&iacute;nio, ou outros materiais como lonas, etc.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sua modelagem pode ser em arco, uma ou mais &quot;&aacute;guas&quot;, shed, piramidal, tesoura, etc. Sua funda&ccedil;&atilde;o deve ser dimensionada, de acordo com as cargas e solicita&ccedil;&otilde;es, observando as caracter&iacute;sticas do solo e utiliza&ccedil;&atilde;o. No seu fechamento lateral deve-se observar a utiliza&ccedil;&atilde;o de materiais adequados a finalidade do mesmo, como tamb&eacute;m aten&ccedil;&atilde;o especial sobre a a&ccedil;&atilde;o de ventos (correntes) internos que causam danos e estacamento de telhas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p>\r\n	Formas de Pagamento:</p>\r\n<p>\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'estruturas-metalicas', 72, NULL, NULL, NULL),
(15, 'Alambrados e Grades de Proteção', '0609201612585926163312..jpg', '<p>\r\n	A tela alambrado &eacute; uma &oacute;tima op&ccedil;&atilde;o para empresas ou resid&ecirc;ncias &nbsp;que possuem &aacute;reas de lazer para funcion&aacute;rios, escolas, clubes e muitos outros locais, pois todas as nossas telas alambrados s&atilde;o produzidas por funcion&aacute;rios de alt&iacute;ssimo n&iacute;vel, treinados e capacitados para o servi&ccedil;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p>\r\n	Formas de Pagamento:</p>\r\n<p>\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>', '', '', '', 'SIM', 0, 'alambrados-e-grades-de-protecao', 74, NULL, NULL, NULL),
(16, 'Portões para Condomínio', '1502201701257703719995..jpg', '<p>\r\n	Os port&otilde;es de enrolar de a&ccedil;o para condom&iacute;nios proporcionam a voc&ecirc; e sua fam&iacute;lia mais acessibilidade na hora de entrar e sair com seu carro em seu condom&iacute;nio, com maior efici&ecirc;ncia e praticidade, utilizando-se da mais alta tecnologia dispon&iacute;vel no mercado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os port&otilde;es de enrolar de a&ccedil;o s&atilde;o fabricadas seguindo os m&eacute;todos mais r&iacute;gidos, permitindo maior robustez, durabilidade e resist&ecirc;ncia. Podendo ser fabricadas de acordo com a solicita&ccedil;&atilde;o de cada cliente, sendo manual ou autom&aacute;tica (motores de alta tecnologia).&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Prazo de Entrega: de 12 a 15 dias</p>\r\n<p>\r\n	Formas de Pagamento:</p>\r\n<p>\r\n	Cheque Pr&eacute;-Datado em 4 vezes ou 3 vezes no Cart&atilde;o de Cr&eacute;dito</p>', '', '', '', 'SIM', 0, 'portoes-para-condominio', 71, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE IF NOT EXISTS `tb_seo` (
  `idseo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idseo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'LIDER PORTAS DE AÇO AUTOMATIZADAS DF - Portas de Aço e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', 'A Lider Portas e Portões atua no mercado de Brasília - DF, oferecendo o que há de melhor em portas de enrolar de aço automatizadas, portões residenciais e industriais, automação, fechaduras elétricas e serralheria, garantindo aos nossos clientes portas de aço com qualidade de alto padrão. Podendo ser instaladas no seu comércio com manutenção quase zero. Nossas portas de aço são as melhores portas existentes no Brasil.', 'A Lider Portas e Portões atua no mercado de Brasília - DF, oferecendo o que há de melhor em portas de enrolar de aço automatizadas, portões residenciais e industriais, automação, fechaduras elétricas e serralheria, garantindo aos nossos clientes portas de aço com qualidade de alto padrão. Podendo ser instaladas no seu comércio com manutenção quase zero. Nossas portas de aço são as melhores portas existentes no Brasil.', 'SIM', NULL, 'index'),
(2, 'Empresa', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', NULL, 'empresa'),
(3, 'Produtos', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', NULL, 'produtos'),
(4, 'Dicas', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', NULL, 'dicas'),
(5, 'Especificações', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', NULL, 'especificacoes'),
(6, 'Portfólio', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', 'Portfólio description', 'Portfólio keywords', 'SIM', NULL, 'portfolio'),
(7, 'Fale Conosco', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', NULL, 'fale-conosco'),
(8, 'Orçamento', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', NULL, 'orcamento'),
(9, 'Trabahe Conosco', 'Lider Portas de Aço Automatizadas e Portões Brasília - DF, Portas de enrolar comercial, Portões de Aço Automáticos.', '', '', 'SIM', NULL, 'trabahe-conosco');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idservico`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
