<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>

<body>

  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  -->
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container-fluid">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->





  <!--  ==============================================================  -->
  <!-- CATEGORIAS PRODUTOS-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row descricao-empresa">
      <div class="col-xs-4 text-center ">
        <h3>PRODUTOS</h3>
      </div>
      <div class="col-xs-8 menu-categorias text-center top15">
        <div id="slider-categorias" class="flexslider slider-categorias">


          <ul class="slides">

            <li>
                <a href="<?php echo Util::caminho_projeto() ?>/produtos/">
                  <img class="pull-left" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-destaques.png" alt="Ver todas">
                  <h4>VER TODAS</h4>
                </a>
            </li>



          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
                <li>
                    <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
                      <img class="pull-left" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                      <h4><?php Util::imprime($row[titulo]); ?></h4>
                    </a>

                </li>
            <?php
            }
          }
          ?>

</ul>
</div>
</div>

</div>
</div>
<!--  ==============================================================  -->
<!-- CATEGORIAS PRODUTOS-->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!--PRODUTOS-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">


   <?php
   $url1 = Url::getURL(1);

  //  FILTRA AS CATEGORIAS
  if (isset( $url1 )) {
      $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
      $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
  }

  //  FILTRA PELO TITULO
  if(isset($_POST[busca_topo])):
    $complemento = "AND titulo LIKE '%$_POST[busca_topo]%'";
  endif;


  $result = $obj_site->select("tb_produtos", $complemento);
   if(mysql_num_rows($result) == 0){
      echo "<p class='bg-danger top20' style='padding: 20px;'>Nenhum produto encontrado.</p>";
    }else{
     while ($row = mysql_fetch_array($result)) {
     ?>
       <div class="col-xs-6 top40">
         <div class="fundo-produto-home">
           <div class="col-xs-12 top15">
            <h1><?php Util::imprime($row[titulo]); ?></h1>
          </div>
          <div class="col-xs-6 top35">
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
            </a>
          </div>
          <div class="col-xs-6 top35">

            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <p><?php Util::imprime($row[descricao], 300); ?></p>
            </a>

            <a class="btn btn-transparente-home" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">ADICIONAR AO ORÇAMENTO</a>
          </div>
        </div>
      </div>
    <?php
    }
  }
  ?>




</div>
</div>


<!--  ==============================================================  -->
<!--PRODUTOS-->
<!--  ==============================================================  -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
