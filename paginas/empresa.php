<?php 


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


  <!-- Jumbo-Slider-2spaltig -->
  <script type="text/javascript">
    jQuery(document).ready(function($) {

      $('#myCarousel').carousel({
        interval: 5000,
      });

      $('#carousel-text').html($('#slide-content-0').html());


  // When the carousel slides, auto update the text
  $('#myCarousel').on('slid.bs.carousel', function (e) {
    var id = $('.item.active').data('slide-number');
    $('#carousel-text').html($('#slide-content-'+id).html());
  });
});
</script>
<!-- Jumbo-Slider-2spaltig -->



</head>

<body>

  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 0) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container-fluid">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->

  

  <!--  ==============================================================  -->
  <!--- barra empresa descricao-->
  <!--  ==============================================================  --> 
  <div class="container">
    <div class="row descricao-empresa">
      <div class="col-xs-4 text-center  ">
        <h3>A EMPRESA</h3>
      </div>
      <div class="col-xs-8">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
        <p class="top10"><?php Util::imprime($row[descricao], 3000); ?></p>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - barra  empresa descricao-->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!-- CONHECA MAIS -->
  <!--  ==============================================================  -->  
  <div class="container">
    <div class="row">
      <div class="col-xs-6">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-triaagulo1.png" alt="">
      </div>
      <div class="col-xs-6 top170 bottom50">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
        <p><?php Util::imprime($row[descricao]); ?></p>
     </div>
   </div>
 </div>
 <!--  ==============================================================  -->
 <!-- CONHECA MAIS -->
 <!--  ==============================================================  -->





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>











