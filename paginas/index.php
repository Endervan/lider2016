<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- ---- LAYER SLIDER ---- -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
  <link href='<?php echo Util::caminho_projeto() ?>/css?family=Lekton|Lobster' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#carousel-gallery").touchCarousel({
        itemsPerPage:5,
        scrollbar: false,
        scrollbarAutoHide: true,
        scrollbarTheme: "dark",
        pagingNav: true,
        snapToItems: true,
        scrollToLast: false,
        useWebkit3d: true,
        loopItems: true
      });
    });
  </script>
  <!-- XXXX LAYER SLIDER XXXX -->


</head>

</head>



<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- slider -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row slider-index">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 1 ");
           if(mysql_num_rows($result) > 0){
            $i = 0;
             while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
             ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
             <?php
              $i++;
             }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner conteudo" role="listbox">

          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
            ?>
                <div class="item <?php if($i == 0){ echo "active"; } ?>">
                  <img class="<?php echo $i ?>-slide" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                    <div class="container">
                      <div class="carousel-caption text-center">

                           <h1><?php Util::imprime($imagem[legenda]); ?></h1>


                          <?php if (!empty($imagem[url])) { ?>
                            <a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>

                          <?php } ?>




                      </div>
                    </div>
                </div>




            <?php
              $i++;
            }
          }
          ?>

          <a class="left carousel-control controles" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control controles" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>

        </div>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- slider -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- categorias  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
     <div class="col-xs-12 text-center">

      <div id="carousel-gallery" class="touchcarousel effect2 black-and-white sombra-categorias">
        <ul class="touchcarousel-container lista-categorias">


          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {

                    //  busca a imagem do produto
                    $result1 = $obj_site->select("tb_produtos", "and imagem <> '' and id_categoriaproduto = $row[0] order by rand() limit 1");
                    if (mysql_num_rows($result1) > 0) {
                      while ($row1 = mysql_fetch_array($result1)) {
                        $row_imagem = $row1[imagem];
                      }
                    }

                    //  conta a qt de produtos
                    $result1 = $obj_site->select("tb_produtos", "and id_categoriaproduto = '$row[0]'");
                    $total_prod = 0;
                    if (mysql_num_rows($result1) > 0) {
                      $total_prod = mysql_num_rows($result1);
                    }


              if (!empty($row_imagem)) {
              ?>
                <li class=" touchcarousel-item">
                 <div class="lista-categoria">
                  <div class="pull-left">
                    <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>">

                      <?php $obj_site->redimensiona_imagem("../uploads/$row_imagem", 99, 150); ?>
                    </a>
                  </div>
                  <div class="desc-categoria pull-right">
                    <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
                      <h1><?php Util::imprime($row[titulo]); ?></h1>
                      <!-- <p class="top20 text-center"><?php //Util::imprime($total_prod ); ?> MODELO(S)</p> -->
                    </a>
                  </div>

                </div>
              </li>
              <?php
              }
            }
          }
          ?>


</ul>
</div>
</div>
</div>
</div>
<!-- ======================================================================= -->
<!-- categorias  -->
<!-- ======================================================================= -->



<!--  ==============================================================  -->
<!-- BARRA HOME ESQUERDA-->
<!--  ==============================================================  -->
<div class="container-fluid top50">
  <div class="row ">
    <div class="bg-lateral-barra"></div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="top20">PRODUTOS EM DESTAQUE</h4>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- BARRA HOME ESQUERDA-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- slider produtos-->
<!-- ======================================================================= -->
<div class="container top20 bottom50">
  <div class="row slider-home">

    <?php
    $result = $obj_site->select("tb_produtos", "order by rand() limit 4");
     if(mysql_num_rows($result) > 0){
       while ($row = mysql_fetch_array($result)) {
       ?>
         <div class="col-xs-6 top40">
           <div class="fundo-produto-home">
             <div class="col-xs-12 top15">
              <h1><?php Util::imprime($row[titulo]); ?></h1>
            </div>
            <div class="col-xs-6 top35">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
              </a>
            </div>
            <div class="col-xs-6 top35">

              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <p><?php Util::imprime($row[descricao], 300); ?></p>
              </a>

              <a class="btn btn-transparente-home" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">ADICIONAR AO ORÇAMENTO</a>
            </div>
          </div>
        </div>
      <?php
      }
    }
    ?>

</div>
</div>
<!-- ======================================================================= -->
<!-- slider PRODUTO-->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!-- CONHECA MAIS -->
<!--  ==============================================================  -->
<div class="container-fluid">
  <div class="row ">
    <div class="bg-triangulo"></div>
    <div class="container">
      <div class="row">

        <div class="col-xs-7 col-xs-offset-5 conheca-mais text-center">
          <h1 class="top50">CONHEÇA MAIS A <strong class="lato-black">LÍDER PORTAS E PORTÕES</strong></h1>
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
          <p class="top25"><?php Util::imprime($row[descricao], 3000); ?></p>
         <a class="btn btn-transparente-home pull-right bottom200" href="<?php echo Util::caminho_projeto() ?>/empresa">VER MAIS</a>
       </div>
     </div>
   </div>
 </div>
</div>

<!--  ==============================================================  -->
<!-- CONHECA MAIS -->
<!--  ==============================================================  -->








<!--  ==============================================================  -->
<!-- BARRA HOME DIREITA-->
<!--  ==============================================================  -->
<div class="container-fluid top100">
  <div class="row ">
    <div class="bg-lateral-barra"></div>
    <div class="container">
      <div class="row">
       <div class="col-xs-6">
        <h4 class="top20">CONFIRA NOSSAS DICAS</h4>
      </div>
      <div class="col-xs-6 text-right">
        <a class="btn btn-transparente-home " href="<?php echo Util::caminho_projeto() ?>/dicas" >VER TODAS</a>
      </div>

    </div>
  </div>
</div>
</div>
<!--  ==============================================================  -->
<!-- BARRA HOME DIREITA-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- DICAS HOME -->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">


    <?php



      $result = $obj_site->select("tb_dicas", "order by rand() limit 3");
      if (mysql_num_rows($result) == 0) {
      ?> <p class="bg-danger top30 p10">Nenhuma dica encontrada.</p> <?php
      }else{
        $i = 0;
        while($row = mysql_fetch_array($result)){
        ?>
          <div class="col-xs-4 text-center top25">
            <div class="thumbnail dicas-home">
              <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                <div class="borda-interna1"></div></a>
                <div class="caption">
                  <p class="top20"><?php Util::imprime($row[titulo]); ?></p>
                <a class="btn btn-transparente-home1 top20 bottom20" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" role="button">SAIBA MAIS</a>
              </div>
            </div>
          </div>
        <?php
        }
      }
      ?>
        </div>
      </div>

      <!--  ==============================================================  -->
      <!-- DICAS HOME -->
      <!--  ==============================================================  -->

      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/rodape.php') ?>
      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->



    </body>

    </html>
