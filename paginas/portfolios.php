<?php 


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>





  <!-- usando carroucel 02 -->
  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel1').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 250,
        itemMargin: 1,
        asNavFor: '#slider1'
      });

      $('#slider1').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 250,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel1"
      });
    });
  </script>


 


</head>

<body>
  <?php require_once('./includes/topo.php'); ?>



  
  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container-fluid">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->

  

  
  <!--  ==============================================================  -->
  <!-- CATEGORIAS PRODUTOS-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row descricao-empresa">
      <div class="col-xs-4 text-center ">
        <h3>PORTIFÓLIO</h3>
      </div>
      <div class="col-xs-8 menu-categorias text-center top15">
        <div id="slider-categorias" class="flexslider slider-categorias">
          

          <ul class="slides">

            <li>
                <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
                  <img class="pull-left" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-destaques.png" alt="Ver todas">
                  <h4>VER TODAS</h4>
                </a>
            </li>

          

          <?php 
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
                <li>
                    <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
                      <img class="pull-left" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                      <h4><?php Util::imprime($row[titulo]); ?></h4>
                    </a>
              
                </li>
            <?php
            }
          }
          ?>

</ul>   
</div>
</div>

</div>
</div>
<!--  ==============================================================  -->
<!-- CATEGORIAS PRODUTOS-->
<!--  ==============================================================  -->





<!--  ==============================================================  -->
<!-- IMAGEM CARROUEL TABS-->
<!--  ==============================================================  -->
<div class="container top20">
  <div class="row">
    <div class="col-xs-12 tabs">
      <div id="slider1" class="flexslider">
        <ul class="slides slider-prod">
          
          
          <?php 
          $result = $obj_site->select("tb_portfolios");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
             <li class="categorias">
               <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" data-toggle="tooltip" data-placement="top" >
                 <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >       
               </a>
             </li>
            <?php 
            }
          }
          ?>

        
         <!-- items mirrored twice, total of 12 -->
       </ul>   
     </div>
   </div>

 </div>
</div>
<!--  ==============================================================  -->
<!-- IMAGEM CARROUEL TABS-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!--TABS USANDO CAROUCEL-->
<!--  ==============================================================  -->
<div class="container-fluid bg-triangulo-portifolio">
  <div class="row">

    <div class="container posicao1 top10">
      <div class="row ">

        <!-- Tab panes -->
        <div class="tab-content">


          <?php 
          $result = $obj_site->select("tb_portfolios", "order by rand() limit 1");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
            <!-- CONTEUDO PORTIFOLIO 01-->
            <div role="tabpanel" class="tab-pane fade in active" id="home">

              <div class="container">
                
                <div class="row dica-dentro">
                  <div class="col-xs-7 top40">
                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                    <p><?php Util::imprime($row[descricao]); ?></p>
                  </div>

                  <div class="col-xs-5 top100">
                    <div class="imagem">    
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 339, 257, array("class"=>"pull-right")) ?>
                        <div class="borda-portifolio"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- CONTEUDO PORTIFOLIO 01-->
            <?php 
            }
          }
          ?>






        </div>
        <!-- Tab panes -->

      </div>
    </div>

    </div>
</div>
<!--  ==============================================================  -->
<!--TABS USANDO CAROUCEL-->
<!--  ==============================================================  -->




<?php require_once('./includes/rodape.php'); ?>

</body>

</html>