<?php


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<body>

  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  -->
  <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
  <style>
      .bg-interna-imagem{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
      }
  </style>
  <div class="container-fluid">
    <div class="row">
      <div class="bg-interna bg-interna-imagem"></div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  background -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--- barra dicas descricao-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row descricao-empresa">
      <div class="col-xs-4 text-center  ">
        <h3> NOSSAS DICAS</h3>
      </div>

      <div class="col-xs-8 top15">

        <form action="<?php echo Util::caminho_projeto() ?>/dicas/" method="post">
          <div class="input-group ">
            <input type="text" class="form-control fundo-form" name="pesquisa_dicas" placeholder="PESQUISA DICAS">
            <span class="input-group-btn ">
              <button class="btn btn-default btn-pesquisa" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </span>
          </div>
        </form>


      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - barra  dicas descricao-->
  <!--  ==============================================================  -->

  <!--  ==============================================================  -->
  <!--- dicas-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">


      <?php

      if (isset($_POST[pesquisa_dicas])) {
        $complemento = "AND titulo LIKE '%$_POST[pesquisa_dicas]%' ";
      }

      $result = $obj_site->select("tb_dicas", $complemento);
      if (mysql_num_rows($result) == 0) {
      ?> <p class="bg-danger top30 p10">Nenhuma dica encontrada.</p> <?php
      }else{
        $i = 0;
        while($row = mysql_fetch_array($result)){
        ?>
          <div class="col-xs-4 text-center top25">
            <div class="thumbnail dicas-home">
              <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                <div class="borda-interna1"></div></a>
                <div class="caption">
                  <p class="top20"><?php Util::imprime($row[titulo]); ?></p>
                <a class="btn btn-transparente-home1 top20 bottom20" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" role="button">SAIBA MAIS</a>
              </div>
            </div>
          </div>
        <?php
        }
      }
      ?>


  </div>
</div>
<!--  ==============================================================  -->
<!-- -  dicas-->
<!--  ==============================================================  -->

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



                  </body>

                  </html>
