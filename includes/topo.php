		

<!--  ==============================================================  -->
<!-- menu usando 2 background como imagens -->
<!--  ==============================================================  -->
<div class="container">
	<div class="row position">
		
		<div class="text-right telefone-topo">
			<h1>
				<i class="fa fa-phone" aria-hidden="true"></i>
				<?php Util::imprime($config[telefone1]); ?>
				<?php if (!empty($config[telefone2])) { ?>
					 / <?php Util::imprime($config[telefone2]); ?>
				<?php } ?>
			</h1>
		</div>

		<div class="col-xs-12 top20">
			<!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
			<div id="navbar">
				<ul class="nav navbar-nav menu text-center">
					<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?> menu-home top20">
						<a href="<?php echo Util::caminho_projeto() ?>" title="HOME">
							<span class="clearfix">HOME</span>
						</a>
					</li>
					<li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?> menu-empresa top20">
						<a href="<?php echo Util::caminho_projeto() ?>/empresa" title="EMPRESA" >
							<span class="clearfix">EMPRESA</span>
						</a>
					</li>
					<li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?> menu-produtos top20">
						<a href="<?php echo Util::caminho_projeto() ?>/produtos" title="PRODUTOS">
							<span class="clearfix">PRODUTOS</span>
						</a>
					</li>
					

					<li>
						<a href="<?php echo Util::caminho_projeto() ?>/" title="Lider Portas e Portões">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="Lider Portas e Portões">									
						</a>
					</li>


					<li class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dicas"){ echo "active"; } ?> menu-dicas top20">
						<a href="<?php echo Util::caminho_projeto() ?>/dicas" title="DICAS">

							<span class="clearfix">DICAS</span>
						</a>
					</li>

					<li class="<?php if(Url::getURL( 0 ) == "especificacoes" or Url::getURL( 0 ) == "especificacao"){ echo "active"; } ?> menu-especificacoes top20">
						<a href="<?php echo Util::caminho_projeto() ?>/especificacoes" title="Especificações">

							<span class="clearfix">ESPECIFICAÇÕES</span>
						</a>
					</li>


					<li class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?> menu-contatos top20">
						<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco" title="Fale Conosco">

							<span class="clearfix">FALE CONOSCO</span>
						</a>
					</li>

				</ul>		          

			</div><!--/.nav-collapse -->
		</div>
	</div>
</div>


<!--  ==============================================================  -->
<!-- menu usando 2 background como imagens -->
<!--  ==============================================================  -->

<!--  ==============================================================  -->
<!-- contatos -->
<!--  ==============================================================  -->
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="bg-contatos">
			<div class="media col-xs-6">
				<div class="media-left">
					<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone.png" height="65px" alt="">
				</div>
				<div class="media-body media-middle telefone-topo">
					<h4 class="media-heading">
						<?php Util::imprime($config[telefone1]); ?>

						<?php if (!empty($config[telefone2])) { ?>
							 / <?php Util::imprime($config[telefone2]); ?>
						<?php } ?>

					</h4>
				</div>
			</div>


			<!-- barra de pesquisas -->
			<div class="dropdown col-xs-3">
				<a href="#" class="dropdown-toggle btn btn-transparente-claro" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-search fa-2x right10"></i>
					<span class="right10">PESQUISAR PRODUTOS</span>
					<span class="caret"></span></a>

					<ul class="dropdown-menu form-busca-topo">
						<form class="navbar-form navbar-left " role="search" method="post" action="<?php echo Util::caminho_projeto() ?>/produtos/">
							<div class="form-group">
								<input type="text" name="busca_topo" class="form-control" placeholder="O que está procurando?">
							</div>
							<button type="submit" class="btn .btn-transparente-claro2">Buscar</button>
						</form>
					</ul>
				</div>
				<!-- barra de pesquisas -->

				<!-- carrinho -->
				<div class="dropdown col-xs-3">
					<a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-meu-orcamento">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-orcamento.png" alt="">
					</a>
					<div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

						<h6 class="bottom20">MEU ORÇAMENTO(<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h6>
							

							<?php
				            if(count($_SESSION[solicitacoes_produtos]) > 0)
				            {
				                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
				                {
				                    $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
				                    ?>
				                    <div class="lista-itens-carrinho">
				                        <div class="col-xs-2">
				                            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
				                        </div>
				                        <div class="col-xs-8">
				                            <h1><?php Util::imprime($row[titulo]) ?></h1>
				                        </div>
				                        <div class="col-xs-1">
				                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
				                        </div>
				                    </div>
				                    <?php
				                }
				            }
				            ?>
			
								

						<div class="text-right bottom20" >
							<a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="Finalizar" class="btn btn-transparente" >
								FINALIZAR
							</a>
						</div>


					</div>

				</div>		
				<!-- carrinho -->
			</div>
		</div>
		</div>
	</div>


	<!--  ==============================================================  -->
	<!-- contatos -->
	<!--  ==============================================================  -->




