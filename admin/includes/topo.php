<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo Util::caminho_projeto() ?>/admin">
        Início
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">

        

        <!-- ======================================================================= -->
        <!-- GOOGLE SEO E BANNERS INTERNAS    -->
        <!-- ======================================================================= -->
        <?php if($_SESSION[login][acesso_tags] == 'SIM'): ?>
        
        <li>
            
              <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                SEO
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="<?php echo Util::caminho_projeto() ?>/admin/seo/index.php">Listar</a></li>
              </ul>
            
        </li>

        <li>
            
              <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Banners Internas
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="<?php echo Util::caminho_projeto() ?>/admin/banners-internas/index.php">Listar</a></li>
              </ul>
            
        </li>


        <?php endif; ?>
        <!-- ======================================================================= -->
        <!-- GOOGLE SEO E BANNERS INTERNAS    -->
        <!-- ======================================================================= -->


        
        <li>
            
              <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Usuários
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="<?php echo Util::caminho_projeto() ?>/admin/login/cadastra.php">Cadastrar</a></li>
                <li><a href="<?php echo Util::caminho_projeto() ?>/admin/login/index.php">Listar</a></li>
              </ul>
            
        </li>
        
        <li><a href="javascript:void(0);">Bem vindo(a), <?php Util::imprime($_SESSION['login']['titulo']); ?></a></li>
        <li><a href="<?php echo Util::caminho_projeto(); ?>/admin/sair.php">Sair</a></li>
      </ul>
    </div>
  </div>
</nav>








