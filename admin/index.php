<?php
session_start();
require_once("../class/Include.class.php");
require_once("login/Login_Model.php");
$obj_login = new Login_Model();

@session_start();
if(isset($_SESSION['login']))
{
  //header("location: inicial.php");  
  Util::script_location("inicial.php");
}


if(isset($_POST['email']))
{
  if($obj_login->efetuar_login($_POST[email], $_POST[senha]) == true)
  {
    //header("location: inicial.php");  
    Util::script_location("inicial.php");
  }
  else
  {
    Util::script_msg("Usuário ou senha incorreto.");
  }
  
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="css/style.css" type="text/css" media="all"/>

<?php require_once("./includes/head.php") ?>

<title>Admin - <?php echo $_SERVER['SERVER_NAME'] ?></title>
</head>

<body class="bg-login">


<div class="container">
	<div class="row top100">
		<div class="col-xs-2 col-xs-offset-3">
			<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" class="input100">
		</div>
		<div class="col-xs-4 text-center top25">
			<h1>Administração do site</h1>
		</div>
	</div>
</div>


<div class="container">
	<div class="row top50">
		<form action="" method="post" name="form" class="FormPrincipal">


    	
        <div class="col-xs-6 col-xs-offset-3 form-group ">
            <label class="glyphicon glyphicon-envelope"> <span>Email</span></label>
            <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
        </div>

        <div class="col-xs-6 col-xs-offset-3 form-group ">
            <label class="glyphicon glyphicon-lock"> <span>Senha</span></label>
            <input type="password" name="senha" class="form-control fundo-form1 input100" placeholder="">
        </div>


        <div class="col-xs-6 col-xs-offset-3">
          
          <div class="pull-left">
            <a href="recupera-senha.php" class="btn btn-warning top10">
              Esqueci minha senha
            </a>
          </div>

          <div class="pull-right">
            <button type="submit" class="btn btn-primary top10">ENVIAR</button>  
          </div>
        	
        </div>



        <input type="hidden" name="action" value="efetuar_login" />
          
    </form>
	</div>
</div>




</body>
</html>




<script>
  $(document).ready(function() {
    $('.FormPrincipal').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {

      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      senha: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>












