<?php
session_start();
require_once("../../class/Include.class.php");
require_once("../trava.php");
require_once("Depoimento_Model.php");


$obj_control = new Depoimento_Model();



//  DESATIVA OU ATIVA UM ITEM
if(isset($_GET[action]) and $_GET[action] == "ativar_desativar")
{
  $obj_control->ativar_desativar($_GET[id], $_GET[ativo]);
  //Util::script_msg("Registro alterado com sucesso");
  Util::script_location("index.php");
}


//  EXCLUIR
if(isset($_GET[action]) and $_GET[action] == "excluir")
{
  $obj_control->excluir($_GET[id]);
  //Util::script_msg("Registro excluído com sucesso.");
  Util::script_location("index.php");
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="css/style.css" type="text/css" media="all"/>

<?php require_once("../includes/head.php") ?>

<title>Admin - <?php echo $_SERVER['SERVER_NAME'] ?></title>
</head>

<body>


	<!-- ======================================================================= -->
	<!-- topo	-->
	<!-- ======================================================================= -->
	<?php require_once("../includes/topo.php"); ?>





	<div class="container-fluid">
      <div class="row">


      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-2 sidebar">
      	<?php require_once("../includes/lateral.php"); ?>
      </div>
      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->





      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-10 main ">
            <?php
                  $result = $obj_control->select();
                  
                  
                  
                  if(mysql_num_rows($result) == 0)
                  {
                        echo "<h1>Nenhum resultado cadastrado</h1>";    
                  }
                  else
                  {
                  ?>
            
            <form action="" method="post" name="form_listagem" id="form_listagem" enctype="multipart/form-data">
                <div class="tabela-holder">
                
                    <table class="table table-hover"> 
                        <thead> 
                            <tr>
                                <th width="34">COD</th>
                                <th width="469">TÍTULO</th>
                                <th width="47">ALBUM</th>
                                <th width="47">EXIBIR</th>
                                <th width="41">EDITAR</th>
                                <th width="54">DELETAR</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                           <?php
                           while($row = mysql_fetch_array($result))
                           {
                           ?>
                            <tr>
                              <td class="titulo-componente-tabela"><?php Util::imprime($row[0]); ?></td>
                              <td class="titulo-componente-tabela"> 
                                                  <?php Util::imprime($row[titulo]); ?>
                                </td>
                                
                                
                                <td align="center">
                                    <?php
                                    $idtabela = "";
                                    $tamanho_max_arquivo = 5000;
                                    $qtd_max_imgs = 1000;
                                    $title = 'Enviar imagens para o album';
                                    $nome_tabela = 'tb_galerias_produtos';
                                    $nome_chave_estrangeira   = 'id_produto';
                                    $nome_campo = 'imagem';
                                    $tamanho_imagem = 750;
                                    $tamanho_tumb = 290;
                                    ?>
                                
                                    <a href="envia_imagens.php?id=<?php echo $row[0]; ?>&tamanho_max_arquivo=<?php echo $tamanho_max_arquivo; ?>&qtd_max_imgs=<?php echo $qtd_max_imgs; ?>&nome_tabela=<?php echo $nome_tabela; ?>&nome_campo=<?php echo $nome_campo; ?>&nome_chave_estrangeira=<?php echo $nome_chave_estrangeira; ?>&tamanho_imagem=<?php echo $tamanho_imagem; ?>&tamanho_tumb=<?php echo $tamanho_tumb; ?>"  data-toggle="tooltip" data-placement="top" title="Enviar imagens para a galeria">
                                        <i class="fa fa-th fa-lg"></i>
                                    </a>
                                </td>
                                
                                <td align="center">
                                    <?php
                                    if($row[ativo] == 'NAO'){ ?>
                                      <a href="?action=ativar_desativar&id=<?php Util::imprime($row[0]); ?>&ativo=<?php Util::imprime($row[ativo]); ?>" data-toggle="tooltip" data-placement="top" title="Desativado" onclick="return confirm('Deseja mesmo ativar o item de código <?php Util::imprime($row[0]); ?> ?')">
                                            <i class="fa fa-eye-slash fa-lg desativado"></i>
                                      </a>
                                    <?php }else{ ?>
                                       <a href="?action=ativar_desativar&id=<?php Util::imprime($row[0]); ?>&ativo=<?php Util::imprime($row[ativo]); ?>" data-toggle="tooltip" data-placement="top" title="Ativado" onclick="return confirm('Deseja mesmo desativar o item de código <?php Util::imprime($row[0]); ?> ?')">
                                            <i class="fa fa-eye fa-lg ativado"></i>
                                      </a> 
                                    <?php } ?>

                                    
                                
                                    
                                </td> 
                                
                                <td align="center">
                                    <a href="altera.php?action=alterar&id=<?php Util::imprime($row[0]); ?>" data-toggle="tooltip" data-placement="top" title="Editar">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                </td> 
                               <td align="center">
                                    <a href="?action=excluir&id=<?php Util::imprime($row[0]); ?>" data-toggle="tooltip" data-placement="top" title="Excluir" onclick="return confirm('Deseja mesmo excluir o item de código <?php Util::imprime($row[0]); ?> ? ')">
                                          <i class="fa fa-trash fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php
                           }
                            ?>
                        </tbody> 
                    </table> 
        </div>
                
                <div class="tabela-fundo">
                    &nbsp;
                </div>
            
            </form>
            <?php
                  }
                  ?>    
      </div>
      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->


        




</body>
</html>